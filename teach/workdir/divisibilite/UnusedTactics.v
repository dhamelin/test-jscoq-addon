Require Setoid.
Require Export ZArith Classical Lia List Znumtheory.
Export BinInt.Z.

(** * Preliminary results and tactics for divisibility exercises *)

(** ** Tactics and lemmas for classical reasoning *)

Tactic Notation "by_cases" constr(A) "as" simple_intropattern(H) :=
  destruct (classic A) as [H | H].

Lemma not_not (A : Prop) : ~ ~ A <-> A.
Proof.
  split; intros H.
  - by_cases A as HA; [exact HA | exfalso; apply H; exact HA].
  - intros HA; apply HA; exact H.
Qed.

Ltac by_contradiction H := apply not_not; intros H.

Lemma not_and (A B: Prop): ~ (A /\ B) <-> ~ A \/ ~ B.
Proof.
  split.
  - intros H. by_cases A as H1.
    + right; intros H2; apply H; split; [exact H1 | exact H2].
    + left; exact H1.
  - intros [H | H] [H1 H2]; apply H; [exact H1 | exact H2].
Qed.

Lemma not_or (A B: Prop) : ~ (A \/ B) <-> ~ A /\ ~ B.
Proof.
  split.
  - intros H; split; intros H'; apply H; [left | right]; exact H'.
  - intros [HnA HnB] [H | H]; [apply HnA | apply HnB]; exact H.
Qed.

Lemma not_impl (A B : Prop) : ~ (A -> B) <-> A /\ ~ B.
Proof.
  split.
  - intros H; by_cases A as HA.
    + split; [exact HA |]; intros HB; apply H; intros _; exact HB.
    + exfalso; apply H; intros HA'; exfalso; apply HA; exact HA'.
  - intros [HA HnB] H; apply HnB; apply H; exact HA.
Qed.

Lemma not_contra (A B : Prop) : (~ A -> ~ B) <-> (B -> A).
Proof.
  split.
  - intros H HB; by_contradiction HA; apply H; [exact HA | exact HB].
  - intros H HA HB; apply HA; apply H; exact HB.
Qed.

(** ** Classical predicate calculus *)

Lemma not_ex {A : Type} (P : A -> Prop) : ~ (exists a, P a) <-> forall a, ~ P a.
Proof.
  split.
  - intros H a Ha; apply H; exists a; exact Ha.
  - intros H [a Ha]; exact (H a Ha).
Qed.

Lemma not_fa {A : Type} (P : A -> Prop) : ~ (forall a, P a) <-> exists a, ~ P a.
Proof.
  split.
  - intros H; by_contradiction H'; apply H; intros a.
    by_contradiction Ha; apply H'; exists a; exact Ha.
  - intros [a Ha] H; apply Ha; exact (H a).
Qed.
  
(** ** Negations of orders *)

Module Z.
Context (a b : Z).
#[local] Open Scope Z_scope.
Lemma not_lt : (~ (a < b)) <-> b <= a. Proof. lia. Qed.
Lemma not_le : (~ (a <= b)) <-> b < a. Proof. lia. Qed.
Lemma not_gt : (~ (a > b)) <-> b >= a. Proof. lia. Qed.
Lemma not_ge : (~ (a >= b)) <-> b > a. Proof. lia. Qed.
End Z.

Module N.
Context (a b : N).
#[local] Open Scope N_scope.
Lemma not_lt : (~ (a < b)) <-> b <= a. Proof. lia. Qed.
Lemma not_le : (~ (a <= b)) <-> b < a. Proof. lia. Qed.
Lemma not_gt : (~ (a > b)) <-> b >= a. Proof. lia. Qed.
Lemma not_ge : (~ (a >= b)) <-> b > a. Proof. lia. Qed.
End N.

Module Nat.
Context (a b : nat).
#[local] Open Scope nat_scope.
Lemma not_lt : (~ (a < b)) <-> b <= a. Proof. lia. Qed.
Lemma not_le : (~ (a <= b)) <-> b < a. Proof. lia. Qed.
Lemma not_gt : (~ (a > b)) <-> b >= a. Proof. lia. Qed.
Lemma not_ge : (~ (a >= b)) <-> b > a. Proof. lia. Qed.
Lemma ge_le_iff : a <= b <-> b >= a. Proof. lia. Qed.
Lemma gt_lt_iff : a < b <-> b > a. Proof. lia. Qed.
End Nat.

(** ** A tactic to push negations *)

#[local]
Ltac srw H := setoid_rewrite H.

#[local]
Ltac gpushneg :=
  repeat (first [
    srw not_not |
    srw not_and |
    srw not_or |
    srw not_impl |
    srw not_contra |
    srw not_ex |
    srw not_fa |
    
    srw Z.not_lt |
    srw Z.not_le |
    srw Z.not_gt |
    srw Z.not_ge |

    srw N.not_lt |
    srw N.not_le |
    srw N.not_gt |
    srw N.not_ge |

    srw Nat.not_lt |
    srw Nat.not_le |
    srw Nat.not_gt |
    srw Nat.not_ge
  ]).

#[local]
Ltac srwi H1 H2 := setoid_rewrite H2 in H1.

#[local]
Ltac hpushneg H :=
  repeat (first [
    srwi H not_not|
    srwi H not_and|
    srwi H not_or|
    srwi H not_impl|
    srwi H not_contra|
    srwi H not_ex|
    srwi H not_fa|
    
    srwi H Z.not_lt |
    srwi H Z.not_le |
    srwi H Z.not_gt |
    srwi H Z.not_ge |

    srwi H N.not_lt |
    srwi H N.not_le |
    srwi H N.not_gt |
    srwi H N.not_ge |

    srwi H Nat.not_lt |
    srwi H Nat.not_le |
    srwi H Nat.not_gt |
    srwi H Nat.not_ge
  ]).

Tactic Notation "pushneg" := gpushneg.
Tactic Notation "pushneg" "in" constr(H) := hpushneg H.

Export Znumtheory ListNotations.

#[local]
Ltac gremoveg :=
  repeat (first [
    srw Nat.ge_le_iff |
    srw Nat.gt_lt_iff |

    srw N.ge_le_iff |
    srw N.gt_lt_iff |

    srw Pos.ge_le_iff |
    srw Pos.gt_lt_iff |

    srw Z.ge_le_iff |
    srw Z.gt_lt_iff
  ]).

#[local]
Ltac hremoveg H :=
  repeat (first [
    srwi H Nat.ge_le_iff |
    srwi H Nat.gt_lt_iff |

    srwi H N.ge_le_iff |
    srwi H N.gt_lt_iff |

    srwi H Pos.ge_le_iff |
    srwi H Pos.gt_lt_iff |

    srwi H Z.ge_le_iff |
    srwi H Z.gt_lt_iff
  ]).

Tactic Notation "removeg" := gremoveg.
Tactic Notation "removeg" "in" constr(H) := hpushneg H.

(** ** Two tactics to prove trivial goals *)

Ltac triv := cbn; try easy; try auto; now lia.

#[local]
Ltac ltriv' H := lapply H; [|triv]; clear H; intro H.
Ltac ltriv H := repeat (ltriv' H).

