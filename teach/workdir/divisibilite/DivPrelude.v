From Coq.ZArith Require Import BinInt Znumtheory Zdiv Wf_Z.
From Coq Require Import Classical.
From Coq Require Import List.
Import ListNotations.

(** * Preliminary lemmas and types for arithmetics exercises *)
#[local] Open Scope Z.

(** ** Missing lemmas about modular arithmetics *)

Lemma div_eucl_0_r (a : Z) : Z.div_eucl a 0 = (0, a).
Proof. destruct a as [| a | a]; reflexivity. Qed.

Lemma div_eucl_0_l (b : Z) : Z.div_eucl 0 b = (0, 0).
Proof. reflexivity. Qed.

(* Alternative: Zdiv.Zmod_0_r *)
Lemma mod_0_r (a : Z) : a mod 0 = a.
Proof.
  pose proof (div_eucl_0_r a) as E.
  unfold "mod"; rewrite E; reflexivity.
Qed.

(* Alternative: Zdiv.Zdiv_0_r *)
Lemma div_0_r (a : Z) : a / 0 = 0.
Proof.
  pose proof (div_eucl_0_r a) as E.
  unfold "/"; rewrite E; reflexivity.
Qed.

(* Alternative: Zdiv.Zmod_0_l, better than Z.mod_0_l *)
Lemma mod_0_l (b : Z) : 0 mod b = 0.
Proof.
  pose proof (div_eucl_0_l b) as E.
  unfold "mod"; rewrite E; reflexivity.
Qed.

(* Alternative: Zdiv.Zdiv_0_l, better than Z.div_0_l *)
Lemma div_0_l (b : Z) : 0 / b = 0.
Proof.
  pose proof (div_eucl_0_l b) as E.
  unfold "/"; rewrite E; reflexivity.
Qed.

(* Better version of Z.div_mod *)
Lemma div_mod (a b : Z) : a = b * (a / b) + a mod b.
Proof.
  destruct (Z.eq_dec b 0) as [-> | E].
  - rewrite div_0_r, mod_0_r; reflexivity.
  - exact (Z.div_mod a b E).
Qed.

(* Better version of Znumtheory.Zmod_divide *)
Lemma mod_divide (a b : Z) : a mod b = 0 -> (b | a).
Proof.
  intros H; exists (a / b); rewrite (div_mod a b) at 1.
  rewrite H, Z.add_0_r, Z.mul_comm; reflexivity.
Qed.

(* Alternative: Znumtheory.Zdivide_mod *)
Lemma divide_mod (a b : Z) : (b | a) -> a mod b = 0.
Proof. intros [k ->]; exact (Zdiv.Z_mod_mult _ _). Qed.

(* Can be useful to reason by equivalence *)
Lemma divide_mod_iff (a b : Z) : (b | a) <-> a mod b = 0.
Proof. split; [exact (divide_mod a b) | exact (mod_divide a b)]. Qed.

(* Better version of Z.add_mod *)
Lemma add_mod (a b n : Z) : (a + b) mod n = (a mod n + b mod n) mod n.
Proof.
  destruct (Z.eq_dec n 0) as [-> | H].
  - rewrite !mod_0_r; reflexivity.
  - exact (Z.add_mod a b n H).
Qed.

(* Currently does not exist in the stdlib, note the unfortunate condition *)
Lemma pow_m1_l_odd (n : Z) : (0 <= n) -> Z.Odd n -> (- 1) ^ n = -1.
Proof.
  intros H H'.
  replace (- 1) with (- (1)) by reflexivity. (* <- ugly *)
  rewrite Z.pow_opp_odd by (exact H').
  rewrite Z.pow_1_l by exact H; reflexivity.
Qed.

(* NOTE: Z.mul_mod but without the unnecessary hypothesis *)
Lemma mul_mod (a b n : Z) : (a * b) mod n = (a mod n * (b mod n)) mod n.
Proof.
  destruct (Z.eq_dec n 0) as [-> | H]; [now rewrite !Zdiv.Zmod_0_r |].
  exact (Z.mul_mod a b n H).
Qed.

(* NOTE: Z.mul_mod_idemp_r but without the unnecessary hypothesis *)
Lemma mul_mod_idemp_r (a b n : Z) : (a * (b mod n)) mod n = (a * b) mod n.
Proof.
  destruct (Z.eq_dec n 0) as [-> | H].
  - rewrite !Zdiv.Zmod_0_r; reflexivity.
  - rewrite Z.mul_mod_idemp_r by (exact H); reflexivity.
Qed.

(* Currently not in the stdlib *)
Lemma pow_mod (a k n : Z) : (a ^ k) mod n = ((a mod n) ^ k) mod n.
Proof.
  destruct (Z.neg_nonneg_cases k).
  - rewrite 2Z.pow_neg_r by (assumption); reflexivity.  (* <- ugly *)
  - apply (Wf_Z.natlike_ind (fun k => a ^ k mod n = (a mod n) ^ k mod n)); (* <- ugly *)
      [rewrite 2Z.pow_0_r; reflexivity | | exact H].
    intros x Hx IH; rewrite 2Z.pow_succ_r, mul_mod, IH by (assumption).
    rewrite mul_mod_idemp_r; reflexivity.
Qed.

Module Nat.
#[local] Open Scope nat_scope.

Fixpoint is_prime_aux (p : nat) (fuel : nat) : bool :=
  match fuel with
  | O => true
  | S fuel' => 
      if (Nat.eqb (Nat.gcd p fuel) 1) then is_prime_aux p fuel' else false
  end.

Definition is_prime (p : nat) : bool :=
  if (leb p 1) then false else (is_prime_aux p (p - 1)).

(* We add the missing definition of prime for a nat, copied from Z for
   consistency, even though it is debatable... *)
Definition rel_prime (a b : nat) :=  Nat.gcd a b = 1.

Inductive prime (p : nat) : Prop :=
    prime_intro : 1 < p ->
                  (forall n : nat, 1 <= n < p -> rel_prime n p) -> prime p.

Lemma Private_test_is_prime :
  Nat.is_prime 0  = false /\
  Nat.is_prime 1  = false /\
  Nat.is_prime 2  = true  /\
  Nat.is_prime 3  = true  /\
  Nat.is_prime 4  = false /\
  Nat.is_prime 5  = true  /\
  Nat.is_prime 6  = false /\
  Nat.is_prime 7  = true  /\
  Nat.is_prime 8  = false /\
  Nat.is_prime 9  = false /\
  Nat.is_prime 10 = false /\
  Nat.is_prime 11 = true  /\
  Nat.is_prime 12 = false /\
  Nat.is_prime 13 = true  /\
  Nat.is_prime 14 = false /\
  Nat.is_prime 15 = false /\
  Nat.is_prime 16 = false /\
  Nat.is_prime 17 = true  /\
  Nat.is_prime 18 = false /\
  Nat.is_prime 19 = true.
Proof. repeat split; reflexivity. Qed.

Lemma is_prime_aux_correct (p : nat) (fuel : nat) :
  is_prime_aux p fuel = false <-> exists n, 1 <= n <= fuel /\ Nat.gcd p n <> 1.
Proof.
  induction fuel as [| fuel' IH].
  - simpl; split; [intros H; discriminate H | intros [n [[H H'] _]]].
    exfalso; apply (Nat.lt_nge 0 1); [exact Nat.lt_0_1 |].
    apply Nat.le_trans with (1 := H); exact H'.
  - simpl; destruct (Nat.eqb (Nat.gcd p (S fuel')) 1) eqn:E.
    + split.
      * intros H; destruct IH as [IH _].
        specialize (IH H) as [n [[H1 Hf] H']]; exists n.
        split; [| exact H']; split; [exact H1 |].
        apply Nat.le_le_succ_r; exact Hf.
      * intros [n [[H1 Hf] H']]; apply IH; exists n; split; [| exact H'].
        split; [exact H1 |].
        apply Nat.lt_eq_cases in Hf as [Hf%(proj1 (Nat.lt_succ_r _ _)) | contra];
          [exact Hf | rewrite contra in H'].
        apply Nat.eqb_neq in H'; rewrite E in H'; discriminate H'.
    + split; intros _; [| reflexivity].
      apply Nat.eqb_neq in E; exists (S fuel'); split; [| exact E].
      split; [| exact (Nat.le_refl _)].
      apply ->Nat.succ_le_mono; exact (Nat.le_0_l _).
Qed.

Lemma contrapose (P Q : Prop) : (~ Q -> ~ P) -> P -> Q.
Proof.
  intros H HQ; apply NNPP; intros HnP; apply H; [exact HnP | exact HQ].
Qed.

Lemma PNNP (P : Prop) : P -> ~ ~ P.
Proof. intros H1 H2; exact (H2 H1). Qed.

(* We lack proper tools for classical reasoning. *)
(* TODO: this is way too painful for a simple contraposition! *)
Lemma is_prime_aux_correct' (p : nat) (fuel : nat) :
  is_prime_aux p fuel = true <-> forall n, 1 <= n <= fuel -> Nat.gcd p n = 1.
Proof.
  split.
  - apply (contrapose (is_prime_aux p fuel = true) (forall (n : nat), _ -> _)).
    intros [n Hn%imply_to_and]%not_all_ex_not.
    enough (is_prime_aux p fuel = false) as P. {
      rewrite P; intros H; discriminate H.
    }
    apply is_prime_aux_correct; exists n; exact Hn.
  - apply (contrapose _ (is_prime_aux _ _ = true)).
    intros [n [Hn Hn']]%Bool.not_true_is_false%is_prime_aux_correct.
    apply ex_not_not_all; exists n; intros H; apply Hn', H; exact Hn.
Qed.

(* TODO: Again, this is way longer than it should be... *)
Lemma is_prime_correct (p : nat) : is_prime p = true <-> prime p.
Proof.
  split.
  - unfold is_prime; intros H.
    destruct (leb p 1) eqn:ineq; [discriminate H |].
    constructor; [apply Nat.leb_gt; exact ineq |]; intros n Hn.
    pose proof (is_prime_aux_correct' p (p - 1)) as [thm _].
    specialize (thm H); unfold rel_prime; rewrite Nat.gcd_comm; apply thm.
    rewrite Nat.sub_1_r; split; [| apply Nat.lt_le_pred]; now apply Hn.
  - intros [Ip Hp]; unfold is_prime; destruct (leb p 1) eqn:Ip'. {
      exfalso; apply ->(Nat.lt_nge 1 p); [exact Ip | apply Nat.leb_le; exact Ip'].
    }
    apply is_prime_aux_correct'; intros n [H1 H2]; rewrite Nat.gcd_comm.
    apply Hp; split; [exact H1 |].
    rewrite Nat.sub_1_r in H2; apply Nat.succ_le_mono in H2.
    rewrite Nat.succ_pred in H2; [apply Nat.le_succ_l; exact H2 |].
    intros C; rewrite C in Ip; apply Nat.lt_asymm with (1 := Ip).
    exact Nat.lt_0_1.
Qed.

Lemma divisor_le_sqrt (n m p : nat) :
  n = m * p -> m <= Nat.sqrt n \/ p <= Nat.sqrt n.
Proof.
  intros H; destruct (Nat.le_ge_cases m p) as
    [I%(Nat.mul_le_mono_l _ _ m) | I%(Nat.mul_le_mono_r _ _ p)]; [left | right];
    rewrite <-H in I; apply Nat.sqrt_le_square in I; exact I.
Qed.

Require Import Lia.
Lemma nprime_has_strict_divisor (p : nat) :
  ~ prime p <-> p <= 1 \/ exists n, 1 < n < p /\ Nat.divide n p.
Proof.
  split.
  - apply contrapose; intros [H1 H2]%not_or_and.
    assert (forall n, 1 < n < p -> ~ Nat.divide n p) as key. {
      apply (not_ex_not_all nat (fun _ => _ -> _)).
      intros [n [Hn1 Hn2%NNPP]%imply_to_and]; apply H2; exists n.
      split; [exact Hn1 | exact Hn2].
    }
    apply PNNP; constructor.
    + apply Nat.nle_gt; exact H1.
    + intros n [[Hn1 | <-]%Nat.le_lteq Hn2]; unfold rel_prime; [| reflexivity].
      enough (~ (Nat.gcd n p = 0) /\ ~ (Nat.gcd n p > 1)) as key' by (lia).
      split; [intros H%Nat.gcd_eq_0_l; lia | intros H].
      apply H2; exists (Nat.gcd n p).
      pose proof (Nat.gcd_divide_r n p) as Hdiv.
      split; [| exact Hdiv].
      destruct Hdiv as [q Hq]; split; [exact H |].
      apply Nat.nle_gt; intros C; rewrite Hq in C at 1.
      rewrite <-(Nat.mul_1_l (Nat.gcd _ _)) in C at 2.
      apply Nat.mul_le_mono_pos_r in C; [| lia].
      assert (Nat.gcd n p <= p) as Hgcd. {
        apply Nat.divide_pos_le; [lia |].
        exact (Nat.gcd_divide_r _ _).
      }
      assert (Nat.gcd n p = p) as eqgcd. {
        apply Nat.le_antisymm; [exact Hgcd |].
        rewrite Hq at 1; rewrite <-(Nat.mul_1_l (Nat.gcd n p)) at 2.
        apply Nat.mul_le_mono_r; exact C.
      }
      pose proof (Nat.gcd_divide_l n p) as H'; rewrite eqgcd in H'.
      apply Nat.divide_pos_le in H'; [| lia].
      lia.
  - intros [H | [n [In Hn]]].
    + intros [H' _]; lia.
    + intros [I Hrel]; specialize (Hrel n).
      assert (Nat.gcd n p = n) as key. {
        apply Nat.divide_antisym. (* <- two spellings of antisymm *)
        * exact (Nat.gcd_divide_l n p).
        * apply Nat.gcd_greatest; [exact (Nat.divide_refl _) | exact Hn].
      }
      unfold rel_prime in Hrel; rewrite key in Hrel; lia.
Qed.

Lemma prime_criterion_sqrt (p : nat) : prime p <->
  1 < p /\ forall n, 1 <= n <= Nat.sqrt p -> Nat.gcd p n = 1.
Proof.
  split.
  - intros [Ip Hp]; split; [exact Ip |]; intros n [Hn Hn'].
    unfold rel_prime in Hp; rewrite Nat.gcd_comm; apply Hp; split.
    + exact Hn.
    + apply Nat.le_lt_trans with (1 := Hn').
      apply Nat.sqrt_lt_lin; exact Ip.
  - intros [Ip Hp]; apply NNPP; intros [I | H]%nprime_has_strict_divisor; [lia |].
    destruct H as [n [In [q Eq]]].
    pose proof (Eq) as Eq'; apply divisor_le_sqrt in Eq as [I | I].
    + assert (1 <= q) as Iq by (lia).
      specialize (Hp q (conj Iq I)).
      assert (Nat.divide q p) as Hdiv. {
        exists n; rewrite Nat.mul_comm; exact Eq'.
      }
      apply Nat.divide_gcd_iff in Hdiv; rewrite Nat.gcd_comm in Hdiv.
      rewrite Hp in Hdiv; lia.
    + assert (Nat.divide n p) as Hdiv by (exists q; exact Eq').
      apply Nat.divide_gcd_iff in Hdiv.
      assert (Nat.gcd n p = 1) as Hgcd. {
        rewrite Nat.gcd_comm; apply Hp; split; [| exact I].
        apply Nat.lt_le_incl; now apply In.
      }
      apply (Nat.lt_irrefl n); lia.
Qed.

Fixpoint is_prime_aux' (p : nat) (fuel : nat) : bool :=
  match fuel with
  | O => true
  | S 0 => true
  | S fuel' => 
      if ((p mod fuel) =? 0) then false else (is_prime_aux' p fuel')
  end.

Definition is_prime' (p : nat) : bool :=
  if (p <=? 1) then false else (is_prime_aux' p (Nat.sqrt p)).

Lemma Private_test_is_prime' :
  Nat.is_prime' 0  = false /\
  Nat.is_prime' 1  = false /\
  Nat.is_prime' 2  = true  /\
  Nat.is_prime' 3  = true  /\
  Nat.is_prime' 4  = false /\
  Nat.is_prime' 5  = true  /\
  Nat.is_prime' 6  = false /\
  Nat.is_prime' 7  = true  /\
  Nat.is_prime' 8  = false /\
  Nat.is_prime' 9  = false /\
  Nat.is_prime' 10 = false /\
  Nat.is_prime' 11 = true  /\
  Nat.is_prime' 12 = false /\
  Nat.is_prime' 13 = true  /\
  Nat.is_prime' 14 = false /\
  Nat.is_prime' 15 = false /\
  Nat.is_prime' 16 = false /\
  Nat.is_prime' 17 = true  /\
  Nat.is_prime' 18 = false /\
  Nat.is_prime' 19 = true.
Proof. repeat split; reflexivity. Qed.
Compute is_prime' 101.
Compute is_prime' 977.
Compute is_prime' 7867.
Compute is_prime' 7869.
Compute is_prime' 65537.
Compute is_prime' 28657.
End Nat.

(* For the moment, we opt for a simple but inefficient primality test in Z. *)
Definition is_prime (p : Z) := Nat.is_prime (Z.to_nat p).

Lemma Private_test_is_prime :
  is_prime (- 5)  = false /\
  is_prime (- 4)  = false /\
  is_prime (- 3)  = false /\
  is_prime (- 2)  = false /\
  is_prime (- 1)  = false /\
  is_prime 0      = false /\
  is_prime 1      = false /\
  is_prime 2      = true  /\
  is_prime 3      = true  /\
  is_prime 4      = false /\
  is_prime 5      = true  /\
  is_prime 6      = false /\
  is_prime 7      = true  /\
  is_prime 8      = false /\
  is_prime 9      = false /\
  is_prime 10     = false /\
  is_prime 11     = true  /\
  is_prime 12     = false /\
  is_prime 13     = true  /\
  is_prime 14     = false /\
  is_prime 15     = false /\
  is_prime 16     = false /\
  is_prime 17     = true  /\
  is_prime 18     = false /\
  is_prime 19     = true.
Proof. repeat split; reflexivity. Qed.

(* TODO: Is it really needed ? *)
Lemma square_opp (x : Z) : (- x) * (- x) = x * x.
Proof. rewrite Z.mul_opp_opp. reflexivity. Qed.

Lemma le_square_r (k : Z) : k <= k * k.
Proof.
  destruct (Z.nonpos_pos_cases k) as [H | H].
  - apply Z.le_trans with (1 := H); exact (Z.square_nonneg k).
  - rewrite <-(Z.mul_1_l k) at 1; apply Z.mul_le_mono_pos_r; [exact H |].
    apply Z.le_succ_l in H; rewrite Z.one_succ; exact H.
Qed.

(* TODO: Is it really needed ? *)
Lemma mod_decomp (a b r : Z) (hb : b <> 0) (h : a mod b = r) :
  exists k, a = b * k + r.
Proof. exists (a / b); rewrite <-h; exact (Z.div_mod a b hb). Qed.

(** ** Numbers as non-empty strings of decimal digits *)

Inductive Digit :=
  d0 | d1 | d2 | d3 | d4 | d5 | d6 | d7 | d8 | d9.

Definition Z_of_Digit (c : Digit) :=
match c with
  d0 => 0
| d1 => 1
| d2 => 2
| d3 => 3
| d4 => 4
| d5 => 5
| d6 => 6
| d7 => 7
| d8 => 8
| d9 => 9
end.

Coercion Z_of_Digit : Digit >-> Z.

Inductive Digits :=
| dsingleton (x : Digit) (* Un seul chiffre | A single digit *)
| dcons (x : Digit) (xs : Digits). (* Un chiffre suivi d'autres chiffres | A digit followed by other digits *)
Coercion dsingleton : Digit >-> Digits. 

(* 
⟪ = Ctrl+Shift+u 27EA 
⟫ = Ctrl+Shift+u 27EB
*)
Notation "⟪ x ⟫" := (dsingleton x).
Notation "⟪ x ; .. ; w ; z ⟫" := (dcons x .. (dcons w (dsingleton z)) ..).

(* When compared to List.length, digits_len returns a Z rather than a nat *)
(* L'avantage par rapport à List.length : on renvoie un Z plutôt qu'un nat *)
Fixpoint digits_len (l : Digits): Z :=
  match l with
  | dsingleton _ => 1
  | dcons _ xs => (digits_len xs) + 1
  end.

Fixpoint digits_sum (cs : Digits) : Z :=
  match cs with 
  | dsingleton d => d
  | dcons x xs => x + (digits_sum xs)
  end.

Theorem digits_sum_suiv (c : Digit) (cs : Digits) : digits_sum (dcons c cs) = c + digits_sum cs.
Proof. reflexivity. Qed.

(* ♯ = Ctrl+Shift+u 266F *)
Notation "♯ x" := (digits_len x) (at level 40). 

Fixpoint Z_of_Digits (l : Digits): Z :=
  match l with
  | dsingleton d => d
  | dcons x xs => x*10^(♯xs) + (Z_of_Digits xs)
  end.

Lemma Z_of_Digits_dsingleton (d : Digit) :
  Z_of_Digits (dsingleton d) = Z_of_Digit d.
Proof. reflexivity. Qed.

Lemma Z_of_Digits_dcons (h : Digit) (t : Digits) :
  Z_of_Digits (dcons h t) = h * 10 ^(♯t) + (Z_of_Digits t).
Proof. reflexivity. Qed.

Fixpoint shiftr (d : Digits) :=
  match d with
  | dsingleton d' => dsingleton d0
  | dcons h (dsingleton d') => dsingleton h
  | dcons h t => dcons h (shiftr t)
  end.

Lemma shiftr_dsingleton (d : Digit) :
  shiftr (dsingleton d) = dsingleton d0.
Proof. reflexivity. Qed.

Lemma shiftr_two_digits (d d' : Digit) :
  shiftr (dcons d (dsingleton d')) = dsingleton d.
Proof. reflexivity. Qed.

Lemma shiftr_digits (h h' : Digit) (t : Digits) :
  shiftr (dcons h (dcons h' t)) = dcons h (shiftr (dcons h' t)).
Proof. reflexivity. Qed.

Lemma digits_len_digit (d : Digit) : digits_len (dsingleton d) = 1.
Proof. reflexivity. Qed.

Lemma digits_len_digits (h : Digit) (t : Digits) :
  digits_len (dcons h t) = (digits_len t) + 1.
Proof. reflexivity. Qed.

Lemma digits_sum_digit (d : Digit) :
  digits_sum (dsingleton d) = d.
Proof. reflexivity. Qed.

Lemma digits_sum_digits (h : Digit) (t : Digits) :
  digits_sum (dcons h t) = h + digits_sum t.
Proof. reflexivity. Qed.

Lemma digits_len_shiftr_digits (h : Digit) (t : Digits) :
  digits_len (shiftr (dcons h t)) = digits_len t.
Proof.
  revert h; induction t as [d | d t' IH].
  - intros h; rewrite shiftr_two_digits, 2!digits_len_digit; reflexivity.
  - intros h; rewrite shiftr_digits, 2digits_len_digits, IH; reflexivity.
Qed.

Lemma digits_len_nneg (d : Digits) : 0 <= digits_len d.
Proof.
  induction d as [d | h t IH].
  - rewrite digits_len_digit; exact (Z.le_0_1).
  - rewrite digits_len_digits; apply Z.add_nonneg_nonneg;
      [exact IH | exact Z.le_0_1].
Qed.

Fixpoint digits_canon (l : Digits) : Digits :=
  match l with 
  | dcons d0 xs => digits_canon xs
  | l => l
  end.

Lemma digits_canon_correct (l : Digits) : Z_of_Digits l = Z_of_Digits (digits_canon l).
Proof.
  induction l as [x | h t IH]; [reflexivity |].
  destruct h; [simpl; exact IH | reflexivity..].
Qed.

Coercion Z_of_Digits : Digits >-> Z.

Definition first_digit (xs : Digits) : Digit :=
  match xs with
  | dsingleton d => d
  | dcons x _ => x
  end.

Fixpoint last_digit (xs : Digits) : Digit :=
  match xs with
  | dsingleton d => d
  | dcons _ xs => last_digit xs
  end.

Lemma last_digit_digit (d : Digit) :
  last_digit (dsingleton d) = d.
Proof. reflexivity. Qed.

Lemma last_digit_digits (h : Digit) (t : Digits) :
  last_digit (dcons h t) = last_digit t.
Proof. reflexivity. Qed.

Lemma decimal_decomposition (n : Digits) :
  Z_of_Digits n = 10 * shiftr n + last_digit n.
Proof.
  induction n as [d | h t IH].
  - reflexivity.
  - destruct t as [d | h' t'].
    + rewrite shiftr_two_digits, last_digit_digits, last_digit_digit.
      rewrite Z_of_Digits_dcons, digits_len_digit, Z.pow_1_r.
      rewrite !Z_of_Digits_dsingleton, Z.mul_comm; reflexivity.
    + rewrite shiftr_digits, Z_of_Digits_dcons, digits_len_digits.
      rewrite (Z_of_Digits_dcons h), last_digit_digits, IH.
      rewrite digits_len_shiftr_digits, Z.add_assoc; f_equal.
      rewrite Z.mul_add_distr_l; f_equal.
      rewrite Z.pow_add_r; [| exact (digits_len_nneg _) | exact Z.le_0_1].
      rewrite Z.pow_1_r, Z.mul_assoc, Z.mul_comm; reflexivity.
Qed.

Lemma digits_canon_nonzero (l : Digits) :
  (digits_canon l) = ⟪ d0 ⟫ \/ first_digit (digits_canon l) <> d0.
Proof.
  induction l as [x | h t [IH0 | IHn0]].
  - destruct x; [left; reflexivity | right; intros H; discriminate H..].
  - destruct h; [left; simpl; exact IH0 | right; intros H; discriminate H..].
  - right; destruct h; simpl; [exact IHn0 | intros H; discriminate H..].
Qed.

Definition findall {E} (P : E -> Prop) := 
  exists (l : list E), Forall P l /\ forall (x : E), P x -> In x l.

Ltac atomize H := destruct H as [H | H]; [| try atomize H].

Definition not_In {E : Type} (x : E) (l : list E) := ~ In x l.

Theorem In_dec {E : Type} (x : E) (l : list E) :
  (In x l) \/ (not_In x l).
Proof.
  induction l as [|y l [IH | IH]]; [right; intros [] |..].
  - left; right; exact IH.
  - destruct (classic (y = x)) as [-> | H].
    + left; left; reflexivity.
    + right; intros [H' | H']; [exact (H H') | exact (IH H')].
Qed.

Fixpoint range_pos (a b : Z) (n: nat) (acc: list Z) {struct n} : list Z :=
  match n with
  | O => a :: acc
  | S n' => range_pos a (b-1) n' (cons b acc)
  end.

Fixpoint range_neg (a b : Z) (n: nat) (acc: list Z) {struct n} : list Z :=
  match n with
  | O => b :: acc
  | S n' => range_neg (a+1) b n' (cons a acc)
  end.

Definition range (a b : Z) : list Z :=
  if a <=? b
  then range_pos a b (Z.to_nat (b-a)) nil
  else range_neg b a (Z.to_nat (a-b)) nil
.                 

#[local]
Ltac false_solv := match goal with
| [H : False |- _ ] => destruct H
end.
                                             
#[local]
Ltac casparcas' H := destruct H as [H | H]; [| try casparcas' H; try false_solv ].

#[local]
Ltac casparcas x l H := 
specialize (In_dec x l) as H; unfold In in H; unfold not_In in H; destruct H as [H|H]; [casparcas' H|].

(*Ltac casparcas_range x a b H := 
specialize (In_dec x (range a b)) as H; remember x as __x' in H; simpl in H; subst __x'; unfold In in H; unfold not_In in H; destruct H as [H|H]; [casparcas' H|]. *)

#[local]
Ltac casparcas_range x a b H := 
  specialize (In_dec x (range a b)) as H; remember x as __x' in H; destruct H as [H|H];
  [ unfold In in H; simpl in H; subst __x'; casparcas' H | unfold not_In in H; simpl in H; subst __x' ].


(*
La tactique `study` est conçue pour rendre plus simple l'analyse des valeurs d'une expression
Si on a un but de la forme Γ ⊢ G, et une expression E : T, alors on peut utilise la tactique
`study E in [a_1,a_2,...,a_n] as H` qui génerera (n+1) buts: le premier est:

Γ,H:E <> a1 /\ E <> a2 /\ ... /\ E <> a_n ⊢ G 
Autrement dit, on doit prouver le but quand on sait que l'expression E
n'est égale à aucun des élements de la liste d'expression qu'on a donné.

Le second but est Γ,H:E = a1 ⊢ G, le troisième Γ,H:E = a2 ⊢ G ... et ainsi de suite jusqu'à Γ,H:E = an ⊢ G

Cette tactique est particulièrement utile dans le contexte des exercices sur la divisibilité, où étudier
les restes possibles d'une division euclidienne est courant pour prouver certaines propriétés.

~~~~

The study tactic is designed to make it easier to analyse the different possibles values of an expression
If we have a goal of the form Γ ⊢ G, and an expression E : T, then we can use the tactic
`study E in [a_1,a_2,...,a_n] as H` to generate (n+1) goals: the first one is

Γ,H:E <> a1 /\ E <> a2 /\ ... /\ E <> a_n ⊢ G : We have to prove the goal when we know that E is not in the list of expression.

The second goal is Γ,H:E = a1 ⊢ G, third is Γ,H:E = a2 ⊢ G and so on until Γ,H:E = an ⊢ G

This tactic is useful in the context of divisibility, where studying the range of remainder is common to
prove some properties.
*)
Tactic Notation "study" constr(x) "in" constr(l) "as" simple_intropattern(H) := casparcas x l H.

(*
`study E between a and b as H` est un sucre syntaxique pour `study E in [a,a+1,a+2,...,b] as H` (si a <= b) ou `[a,a-1,a-2,...,b]` (si a > b)

Il est courant d'analyser des intervalles continus lorsqu'on travaille sur la divisibilité:
Ce raccourcis permet d'éviter de faire des erreurs en listant les valeurs possibles d'un intervalle.

~~~

`study E between a and b as H` is a syntax sugar for `study E in [a,a+1,a+2,...,b] as H` (if a <= b) or [a,a-1,a-2,...,b] (if a > b)

It is common to have contiguous ranges when working with divisibility:
This syntax makes it less error-prone to list all possible values in a range.
*)
Tactic Notation "study" constr(x) "between" constr(a) "and" constr(b) "as" simple_intropattern(H) := casparcas_range x a b H.
