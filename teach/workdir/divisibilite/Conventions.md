Style :

- En général se rapprocher de ce qui est affiché par Coq parce que c'est moins
  perturbant pour les étudiants/utilisateurs (et en plus, je trouve ça plus
  joli), par exemple : (a : nat) au lieu de (a: nat) ou (a :nat)
- On écrit des espaces entre les opérateurs binaires : (a ^ 3) au lieu de (a^3)
- Indenter à deux espaces, y compris dans les preuves
- Pas de ligne > 80 caractère
- Toujours prouver un sous-but avec une closing tactic (reflexivity, assumption,
  exact, ou au pire now ou easy)
- Donner les lemmes qui permettent de se passer de simpl, par exemple
  `digits_len_digit`, `add_0_l`, ...
- Lorsqu'une preuve a des sous-partie identifiables de taille copmparable,
  utiliser des bullets, ordre : `-` puis `+` puis `*`.
- Il ne doit pas y avoir plus de trois niveaux (sinon, c'est qu'on a raté une
  preuve trop petite pour un niveau).
- Une sous preuve rapide ou de taille non comparable au reste est prouvée
  directement, avec par exemple :
  assert (2 = 2) as plop. {
    reflexivity.
  }
  ou bien, si possible
  assert (2 = 2) as plop by (reflexivity).
  En général, comme dans mathcomp, on essaie de se débarasser des petits
  sous-buts en premier. On peut utiliser `; cycle 1` si un tel sous-but est en
  deuxième (c'est le cas pour les sous-buts générés par rewrite) pour éviter
  qu'il y ait trop de sous-buts.
- On focus toujours la preuve sur un seul sous-but.
- Sélecteurs avec modération, mais possibles si ça simplifie la preuve (ou
  qu'on ne peut s'en passer).
- C'est important de limiter le nombre de tactiques utilisées, parce que plus
  on en ajoute, plus on risque de perdre les étudiants.
- intros : toujours nommer (sauf si ça devient très très ennuyeux et dans ce cas
  automatiser et utiliser sans nommage)
- Idéalement, une preuve doit être lisible sans Coq.
- Tactiques à privilégier :
  - rewrite, apply, split, left, right, exists, exact, intros, reflexivity
  - (destruct), specialize, assert, replace, pose proof, (assumption)
- On identifie toujours les étapes qui sont du bruit ajouté par une mauvaise
  formalisation ou des problèmes liés à Coq et on laisse un commentaire pour
  pouvoir éventuellement y revenir plus tard.
- Beaucoup de tactiques vanilla ont la syntaxe commune
  `tactique truc_muche as intro_pattern [by (preuve)]`
  C'est joli comme ça et ça rend les choses plus régulières, donc on évite
  ```
  assert (A := plop).
  destruct a. (* sans rien introduire, sauf si c'est une preuve de faux *)
  ```
- Une seule ligne vide entre deux lemmes.
- Aucun trailing whitespace.
- Un seul caractère fin de ligne à la fin du fichier.
