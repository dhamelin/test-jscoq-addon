Inductive Conv : forall T,  T -> Type :=
| conv_intro {T} {x:T} : Conv T x.

Lemma conv_left {A B C:Type} {P: A -> B -> C} {a : A} {b : B} : Conv Type C -> Conv (P a b).
  intro h.
  apply conv_intro.
Qed.

Lemma conv_right {A B:Type} {P: A -> B -> Type} {a : A} {b : B} : Conv b -> Conv (P a b).
  intro h.
  apply conv_intro.
Qed.



Goal forall (n m : nat), Conv ((n+1 = m) \/ (n<>m)).
  intros n m.
  Check (Conv ((n+1 = m) \/ (n<>m))).
  apply @conv_left with (P := or).
