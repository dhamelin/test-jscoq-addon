(** Les ensembles comme sigtype *)
Require Import PeanoNat.
Section EnsemblesSigType.
  Check Nat.Even.
  Definition EntiersPairs := { n : nat | Nat.Even n }.
  Coercion val (x : EntiersPairs) : nat := (proj1_sig x).
  Definition two_even : EntiersPairs.
  Proof.
    exists 2.
    exists 1.
    reflexivity.
  Defined.
  Compute (two_even + 1)%nat.
  Check EntiersPairs.
End EnsemblesSigType.

