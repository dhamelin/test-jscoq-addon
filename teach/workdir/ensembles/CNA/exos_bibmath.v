From Coq Require Import Arith ZArith_base Reals Lra.
From Coq Require Import ssreflect ssrfun.

Set Warnings "-notation-overridden".
From Lebesgue Require Import Subset Subset_dec Function.
Set Warnings "notation-overridden".

From FEM Require Import Compl.


(** Exercises on sets, functions and relations.
 Taken from https://www.bibmath.net/ressources/index.php?action=affiche&quoi=mathsup/feuillesexo/ensembleapplicationrelation&type=fexo *)


Open Scope nat_scope.
Section Exo_3_1.

Let f := fun n => n + 1.

Lemma f_inj : injective f.
Proof.
unfold f.
intros x y.
rewrite !Nat.add_1_r.
apply Nat.succ_inj.
Qed.

Lemma f_not_surj : ~ surjective f.
Proof.
unfold f.
rewrite not_all_ex_not_equiv. (* or use 'apply'. *)
exists 0.
rewrite not_ex_all_not_equiv. (* or use 'apply'. *)
intros n.
rewrite Nat.add_1_r.
apply Nat.neq_succ_0.
Qed.

End Exo_3_1.
Close Scope nat_scope.


Open Scope Z_scope.
Section Exo_3_2.

Lemma g_bij : bijective (fun n => n + 1).
Proof.
apply Bijective with (g := fun n => n - 1); intros n.
(* 'ring' solves next two goals. *)
apply Z.add_simpl_r.
apply Z.sub_add.
Qed.

End Exo_3_2.
Close Scope Z_scope.


Open Scope R_scope.
Section Exo_3_3.

Let h := fun X : R * R =>
  let (x, y) := X in
  (x + y, x - y).

Lemma h_bij : bijective h.
Proof.
unfold h.
apply bij_ex_uniq_rev.
intros [x y].
exists ((x + y) / 2, (x - y) / 2); split.
+ f_equal.
  - rewrite -Rdiv_plus_distr.
    rewrite Rplus_assoc.
    rewrite Rplus_minus.
    rewrite Rdiv_plus_distr.
    apply eq_sym.
    apply double_var. (* note that 'lra' does it all! *)
  - lra.
+ intros [x1 y1].
  rewrite pair_equal_spec; intros [H1 H2]; rewrite -H1 -H2.
  f_equal; lra.
Qed.

End Exo_3_3.
Close Scope R_scope.


Open Scope R_scope.
Section Exo_4_1.

Let f := fun x => Rsqr x.
Let A := fun x => -1 <= x <= 4.

Lemma image_of_A : image f A = fun x => 0 <= x <= 16.
Proof.
apply subset_ext_equiv; split; intros y Hy.
+ induction Hy as [x [Hx1 Hx2]]; split.
  - apply Rle_0_sqr.
  - case (Rle_lt_dec 0 x); intros Hx.
    * replace 16 with (f 4); [| unfold f, Rsqr; lra].
      apply Rsqr_le_abs_1.
      rewrite !Rabs_pos_eq; [assumption | lra | assumption].
    * apply Rle_trans with 1; [| lra].
      replace 1 with (f 1); [| unfold f, Rsqr; lra].
      apply Rsqr_le_abs_1.
      rewrite Rabs_left; [| assumption].
      rewrite Rabs_pos_eq; lra.
+ destruct Hy as [Hy1 Hy2].
  rewrite image_eq; exists (sqrt y); split.
  - unfold A; split.
    * apply Rle_trans with 0; [lra |].
      apply sqrt_positivity; assumption.
    * replace 4 with (sqrt 16).
      apply sqrt_le_1_alt; assumption.
      replace 16 with (Rsqr 4); [| unfold Rsqr; lra].
      rewrite sqrt_Rsqr_abs; apply Rabs_pos_eq; lra.
  - unfold f; rewrite Rsqr_sqrt; [reflexivity | assumption].
Qed.

Lemma preimage_of_A : preimage f A = fun x => -2 <= x <= 2.
Proof.
apply subset_ext_equiv; split; intros y [Hy1 Hy2].
+ replace 4 with (Rsqr 2) in Hy2; [| unfold Rsqr; lra].
  split.
  - apply Rsqr_neg_pos_le_0; replace (IPR 2) with 2;
        [assumption | reflexivity | lra | reflexivity].
  - apply Rsqr_incr_0_var; [assumption | lra].
+ split.
  - apply Rle_trans with 0; [lra |].
    apply Rle_0_sqr.
    replace 4 with (f 2); [| unfold f, Rsqr; lra].
    apply neg_pos_Rsqr_le; assumption.
Qed.

(* Naming of lemmas about real numbers in the Coq standard library does not
 seem to follow reasonable rules:
 - 'Ropp_le_cancel' could be something as 'Ropp_monot' or 'Ropp_nonincr',
 - 'Rabs_pos_eq' and 'Rabs_left' should be closer,
 - 'Rle_0_sqr' should be something as 'Rsqr_nonneg' or 'Rsqr_ge_0',
 - 'Rsqr_le_abs_1' is weird,
 - 'Rsqr_incr_0_var' and 'Rsqr_neg_pos_le_0' should be closer,
 - 'sqrt_le_1_alt' should be something as 'sqrt_monot' or 'sqrt_nondecr',
 - 'sqrt_Rsqr_abs' could simply be 'sqrt_Rsqr',

 It would be nice that 'lra' knows Rsqr.
*)

End Exo_4_1.
Close Scope R_scope.


Section Exo_14.

Context {E : Type}.
Variable A B C : E -> Prop.

Lemma exo_14_1 : union (inter A B) C = inter (union A C) (union B C).
Proof. (* 'apply union_inter_distr_r' solves this! *)
apply subset_ext_equiv; split.
+ intros x [[Hx1 Hx2] | Hx]; split.
  1,2: left; assumption.
  1,2: right; assumption.
+ intros x [[Hx1 | Hx1] [Hx2 | Hx2]].
  left; split; assumption.
  1,2,3: right; assumption.
Qed.

Lemma exo_14_2 : compl (compl A) = A.
Proof. (* 'apply compl_invol' solves this! *)
apply subset_ext; intros x; apply NNPP_equiv.
Qed.

Lemma exo_14_3 : compl (inter A B) = union (compl A) (compl B).
Proof. (* 'apply compl_inter' solves this! *)
apply subset_ext_equiv; split.
+ intros x Hx; apply not_and_equiv in Hx.
  destruct Hx as [Hx | Hx].
  left; assumption.
  right; assumption.
+ intros x [Hx1 | Hx1] [Hx2 Hx3].
  1,2: apply Hx1; assumption.
Qed.

Lemma exo_14_4 : compl (union A B) = inter (compl A) (compl B).
Proof. (* 'apply compl_union' solves this! *)
apply subset_ext; intros x.
unfold compl; rewrite not_or_equiv.
unfold inter.
apply iff_refl.
Qed.

End Exo_14.


Open Scope R_scope.
Section Exo_15.

Let D := fun X : R * R =>
  let (x, y) := X in
  Rsqr x + Rsqr y <= 1.

Lemma exo_15 : forall (A B : R -> Prop), D <> prod A B.
Proof.
intros A B H.
assert (H1 : D (1, 0)).
+ unfold D; rewrite Rsqr_1 Rsqr_0; lra.
assert (H2 : D (0, 1)).
+ unfold D; rewrite Rsqr_1 Rsqr_0; lra.
assert (H3 : prod A B (1, 1)).
+ split; simpl.
  - rewrite H in H1.
    destruct H1 as [H1 _].
    simpl in H1.
    assumption.
  - rewrite H in H2; destruct H2 as [_ H2]; assumption.
contradict H3.
rewrite -H.
unfold D; rewrite Rsqr_1; lra.
Qed.

End Exo_15.
Close Scope R_scope.


Section Exo_16a.

Context {E : Type}.
Variable A B C : E -> Prop.

Lemma Exo_16_1 : inter A B = union A B -> A = B.
Proof. (* 'apply inter_eq_union' solves this! *)
assert (H0 : forall P Q : E -> Prop, inter P Q = union P Q -> incl P Q).
+ intros P Q H x Hx.
  destruct (in_dec Q x) as [Hx1 | Hx1]; [assumption | exfalso].
  contradict H; apply subset_ext_contra_rev.
  exists x; rewrite compl_inter.
  right; split; [right | left]; assumption.
intros H; apply subset_ext_equiv; split; apply H0.
assumption.
rewrite inter_comm union_comm; assumption.
Qed.

Lemma Exo_16_2a : inter A B = inter A C -> union A B = union A C -> B = C.
Proof. (* 'apply inter_union_inj_r' solves this! *)
assert (H0 : forall P Q R : E -> Prop,
    inter P Q = inter P R -> union P Q = union P R -> incl Q R).
+ intros P Q R H1 H2 x Hx1.
  destruct (in_dec P x) as [Hx2 | Hx2].
  - apply (inter_lb_r P _ x); rewrite -H1; split; assumption.
  - apply (inter_union_compl_l P x); split; [rewrite -H2; right |]; assumption.
intros H1 H2; apply subset_ext_equiv; split; apply (H0 A);
    [| | apply eq_sym..]; assumption.
Qed.

End Exo_16a.


Open Scope nat_scope.
Section Exo_16b.

Lemma Exo_16_2b :
  ~ (forall {E : Type} (A B C : E -> Prop), inter A B = inter A C -> B = C).
Proof.
rewrite not_all_ex_not_equiv; exists nat.
rewrite not_all_ex_not_equiv; exists (singleton 1).
rewrite not_all_ex_not_equiv; exists (singleton 1).
rewrite not_all_ex_not_equiv; exists (pair 1 2).
rewrite not_imp_and_equiv; split.
+ rewrite !inter_singleton_l_in; [reflexivity |..].
  - apply pair_in_l.
  - apply singleton_in.
+ apply subset_ext_contra_rev.
  exists 2; right; split.
  - apply not_eq_sym, Nat.lt_neq, Nat.lt_1_2.
  - apply pair_in_r.
Qed.

Lemma Exo_16_2c :
  ~ (forall {E : Type} (A B C : E -> Prop), union A B = union A C -> B = C).
Proof.
rewrite not_all_ex_not_equiv; exists nat.
rewrite not_all_ex_not_equiv; exists (pair 1 2).
rewrite not_all_ex_not_equiv; exists (singleton 1).
rewrite not_all_ex_not_equiv; exists (singleton 2).
rewrite not_imp_and_equiv; split.
+ rewrite !union_left; [reflexivity |..].
  intros x; apply pair_in_r_alt.
  intros x; apply pair_in_l_alt.
+ apply subset_ext_contra_rev.
  exists 1; left; split.
  - apply singleton_in.
  - apply Nat.lt_neq, Nat.lt_1_2.
Qed.

End Exo_16b.
Close Scope nat_scope.


Section Exo_17.

Context {E : Type}.

Definition diff_sym (A B : E -> Prop) : E -> Prop :=
  diff (union A B) (inter A B).
(* This corresponds to the equivalent definition of sym_diff
 given in 'sym_diff_equiv_def_diff'... *)

Lemma exo_17_2 :
  forall (A B : E -> Prop), diff_sym A B = union (diff A B) (diff B A).
Proof.
(* Proof using properties of sym_diff.
intros; apply eq_sym, sym_diff_equiv_def_diff.
 *)
(* Proof not using properties of sym_diff.
intros; unfold diff_sym; rewrite diff_inter_distr_r.
rewrite diff_union_l_diag diff_union_r_diag.
apply union_comm.
 *)
(* Proof only using properties of inter, union and compl. *)
intros; unfold diff_sym, diff.
rewrite compl_inter.
rewrite inter_union_distr_r.
rewrite !inter_union_distr_l.
rewrite !inter_compl_r.
rewrite union_empty_l union_empty_r.
reflexivity.
Qed.

Lemma exo_17_3a : forall (A : E -> Prop), diff_sym A A = emptyset.
Proof.
(* Proof using properties of sym_diff.
intros; unfold diff_sym; rewrite -sym_diff_equiv_def_diff.
apply sym_diff_inv.
 *)
(* Proof not using properties of sym_diff.
intros; rewrite exo_17_2.
rewrite diff_empty_diag.
apply union_empty_l.
 *)
(* Proof only using properties of inter, union and compl. *)
intros; rewrite exo_17_2; unfold diff.
rewrite inter_compl_r.
apply union_empty_l.
Qed.

Lemma exo_17_3b : forall (A : E -> Prop), diff_sym A emptyset = A.
Proof.
(* Proof using properties of sym_diff.
intros; unfold diff_sym; rewrite -sym_diff_equiv_def_diff.
apply sym_diff_empty_r.
 *)
(* Proof not using properties of sym_diff.
intros; rewrite exo_17_2.
rewrite diff_empty_r diff_empty_l.
apply union_empty_r.
 *)
(* Proof only using properties of inter, union and compl. *)
intros; rewrite exo_17_2; unfold diff.
rewrite compl_empty inter_full_r inter_empty_l.
apply union_empty_r.
Qed.

Lemma exo_17_3c : forall (A : E -> Prop), diff_sym A fullset = compl A.
Proof.
(* Proof using properties of sym_diff.
intros; unfold diff_sym; rewrite -sym_diff_equiv_def_diff.
apply sym_diff_full_r.
 *)
(* Proof not using properties of sym_diff.
intros; rewrite exo_17_2.
rewrite diff_full_r diff_full_l.
apply union_empty_l.
 *)
(* Proof only using properties of inter, union and compl. *)
intros; rewrite exo_17_2; unfold diff.
rewrite compl_full inter_empty_r inter_full_l.
apply union_empty_l.
Qed.

Lemma exo_17_3d : forall (A : E -> Prop), diff_sym A (compl A) = fullset.
Proof.
(* Proof using properties of sym_diff.
intros; unfold diff_sym; rewrite -sym_diff_equiv_def_diff.
apply sym_diff_compl_r.
 *)
(* Proof not using properties of sym_diff.
intros; rewrite exo_17_2.
rewrite diff_compl_r_diag diff_compl_l_diag.
apply union_compl_r.
 *)
(* Proof only using properties of inter, union and compl. *)
intros; rewrite exo_17_2; unfold diff.
rewrite compl_invol !inter_idem.
apply union_compl_r.
Qed.

Lemma exo_17_4 :
  forall (A B C : E -> Prop),
    inter (diff_sym A B) C = diff_sym (inter A C) (inter B C).
Proof.
(* Proof using properties of sym_diff.
intros; unfold diff_sym; rewrite -!sym_diff_equiv_def_diff.
apply inter_sym_diff_distr_l.
 *)
(* Proof not using properties of sym_diff.
intros; rewrite !exo_17_2.
rewrite -!inter_diff_distr_r.
apply inter_union_distr_r.
 *)
(* Proof only using properties of inter, union and compl. *)
intros; rewrite !exo_17_2; unfold diff.
rewrite inter_union_distr_r.
rewrite !compl_inter.
f_equal.
+ rewrite inter_union_distr_l.
  rewrite !inter_assoc.
  rewrite inter_compl_r inter_empty_r union_empty_r.
  rewrite (inter_comm C).
  reflexivity.
+ rewrite inter_union_distr_l.
  rewrite !inter_assoc.
  rewrite inter_compl_r inter_empty_r union_empty_r.
  rewrite (inter_comm C).
  reflexivity.
Qed.

End Exo_17.


Section Exo_18.

Context {E : Type}.

Lemma exo_18 : forall (A B : E -> Prop), sym_diff A B = B <-> A = emptyset.
Proof. (* 'exact sym_diff_eq_r' solves this! *)
(* Proof not using properties of sym_diff.
intros A B; unfold sym_diff; split.
+ intros H.
  rewrite -(inter_full_r A) -(union_compl_r B) inter_union_distr_l.
  apply union_empty; split.
  - apply disj_equiv_def, disj_sym, diff_disj.
    apply subset_ext; split.
    + apply diff_lb_l.
    + intros Hx; split; [assumption |].
      assert (Hx1 : B x) by assumption.
      rewrite -H in Hx1; destruct Hx1 as [[_ Hx1] | [_ Hx1]].
      - contradict Hx; assumption.
      - assumption.
  - apply empty_emptyset; intros x Hx.
    generalize (union_ub_l _ (diff B A) _ Hx); rewrite H.
    destruct Hx as [_ Hx]; assumption.
+ intros; subst.
  rewrite diff_empty_l diff_empty_r.
  apply union_empty_l.
 *)
(* Proof only using properties of inter, union and compl. *)
intros A B; unfold sym_diff, diff; split.
+ intros H.
  rewrite -(inter_full_r A) -(union_compl_r B) inter_union_distr_l.
  apply union_empty; split.
  - apply inter_empty_r_alt, inter_left.
    apply subset_ext; split.
    + intros [Hx _]; assumption.
    + intros Hx; split; [assumption |].
      assert (Hx1 : B x) by assumption.
      rewrite -H in Hx1; destruct Hx1 as [[_ Hx1] | [_ Hx1]].
      - contradict Hx; assumption.
      - assumption.
  - apply empty_emptyset; intros x Hx.
    generalize (union_ub_l _ (inter B (compl A)) _ Hx); rewrite H.
    destruct Hx as [_ Hx]; assumption.
+ intros; subst.
  rewrite compl_empty inter_empty_l inter_full_r.
  apply union_empty_l.
Qed.

End Exo_18.
