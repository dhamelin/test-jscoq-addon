From mathcomp Require Import all_ssreflect ssralg matrix finmap order ssrnum.
From mathcomp Require Import ssrint interval ssrnat.
From mathcomp Require Import mathcomp_extra boolp.
From mathcomp Require Import classical.classical_sets.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Open Scope classical_set_scope.

Context {T : Type}.
(** ** Vocabulaire et notations. *)

About set.
Check set T.

(** set T est synonyme de T -> Prop *)
Print set.

(** L'appartenance est notée \in : elle vit dans [bool]. *)
Lemma in_bool_Prop (A : set T) (x : T) : x \in A <-> A x.
Proof.
  split.
  (* Utiliser la vue /asboolP pour passer du monde booléen au monde de Prop. *)
  - move /asboolP. done.
  - move /asboolP.
    (* Il semble que `[< A x >] est une notation pour asbool.
       Les deux côtés de l'implication sont de toute façon convertibles. *)
    by apply.
Qed.

(** Il y a une notation \notin la non-appartenance. *)
Lemma notin_bool_Prop (A : set T) (x : T) : x \notin A <-> ~ A x.
Proof.
  split.
  - move /asboolP. done.
  - move =>H.
    (* On peut appliquer la vue /asboolPn pour passer d'une négation booléenne
       à une négation dans Prop. *)
    apply /asboolPn. done.
Qed.

(** Pour créer un ensemble à partir d'un prédicat, on utilise [set x : T | P].
 * *)
Search _ inside ssrnat.
Lemma example_even : 4 \notin [set x : nat | odd x].
Proof.
  apply /asboolPn.
  (* mksetE permet de passer explicitement de l'ensemble au prédicat associé. *)
  rewrite mksetE.
  (* En fait odd de ssrnat est une fonction booléenne qui calcule.
     Mais en cours de mathématiques, on a peut-être envie de dire : *)
  replace 4 with (2.*2) by done.
  rewrite odd_double.
  done.
Qed.

(** L'ensemble total est setT *)

Print setT.

Lemma all_in_all (x : T) : x \in setT.
Proof.
  (* mksetE ne marche pas, je ne sais pas pourquoi *)
  by apply: mem_set.
Qed.

(** L'ensemble vide est set0. *)

Print set0.

Lemma noone_in_empty (x : T) : x \notin set0.
Proof.
  rewrite /set0. by apply /asboolPn.
Qed.

(** Pour être honnête, je suis loin de comprendre la moitié de ce que j'écris.
    Ça risque d'être compliqué avec des étudiants.

    C'est peut-être prématuré de se lancer sans une formation mathcomp.
    Ceci dit, il y a sans doute des choses à prendre. *)

Context (A B : set T) (x : T).
