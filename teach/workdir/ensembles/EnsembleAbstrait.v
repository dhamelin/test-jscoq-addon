(* Un ensemble fini est un ensemble + une preuve qu'il
 est composé d'une répétition d'addition *)

(* On peut astucieusement faire ça avec une notation *)

(* Les types classes permettent de les traiter comme des ensembles normaux *)
Require List.


(*
  Un ensemble est composé d'une fonction indicatrice dans Prop,
  et éventuellement d'une preuve que l'ensemble est fini:
  L'ensemble est fini s'il existe une liste qui permet de simuler
  le comportement de la fonction indicatrice.

  Avec proof irrelevance et functional extensionality l'égalité de Coq
  correspond à l'égalité entre ensemble usuelle
*)

Record Ensemble (T:Type) := mkset {
  f :> T -> Prop;
  finProof: option (exists (l: list T), f = (fun x => List.In x l));
}.

Definition type_to_ens (T:Type) : Ensemble T := mkset T (fun (x:T) => True) None.
Coercion type_to_ens : Sortclass >-> Ensemble.


Class SetIn (A B: Type) := {set_in : A -> B -> Prop}.
Class Intersection (A B C : Type) := {set_intersection : A -> B -> C }.

  (* Types d'ensembles:
- 

   *)

Module Type AEnsemble.
  Import M.
  
  Parameter Ensemble : Type. (* Peut-être Prop ? à voir *)
  Parameter In : Any -> Ensemble -> Prop.
  Parameter intersection : Ensemble -> Ensemble -> Ensemble.
  Parameter union : Ensemble -> Ensemble -> Ensemble.
  Parameter difference : Ensemble -> Ensemble -> Ensemble.
  Parameter subset : Ensemble -> Ensemble -> Prop.
  Parameter cartesian : Ensemble -> Ensemble -> Ensemble.
  Parameter powerset : Ensemble -> Ensemble.
(*
  (* J'ai un doute pour ça, beaucoup d'axiomatisation possible *)
  Parameter Injection : forall (T1 T2: Type), T1 -> T2 -> Prop.
  Arguments Injection {T1 T2}.

  Parameter Surjection : forall (T1 T2: Type), T1 -> T2 -> Prop.
  Arguments Surjection {T1 T2}.
  
  Parameter Bijection : forall (T1 T2: Type), T1 -> T2 -> Prop.
  Arguments Bijection {T1 T2}.
*)
End AEnsemble.



Module Type AEnsembleNotations (Import M: AEnsemble).
  Declare Scope ens_scope.
  Infix "∈" := (In) (at level 40) : ens_scope.
  Infix "∩" := (intersection) (at level 40) : ens_scope.
  Infix "∪" := (union) (at level 40) : ens_scope.
  Infix "\" := (difference) (at level 40) : ens_scope.
  Infix "⊂" := (subset) (at level 40) : ens_scope.
  Notation "x ⊆ y" := (subset x y \/ x = y) (at level 40) : ens_scope.
  Infix "×" := (cartesian) (at level 40) : ens_scope.
End AEnsembleNotations.

Module Type AEnsembleFacts (Import M: AEnsemble).
  Module Aux.
    Include AEnsembleNotations M.
  End Aux.

  (* Flippant, les scope leak des modules! *)
  Import Aux.
  Local Open Scope ens_scope.
  
  Parameter intersection_spec :
    forall (e1 e2:Ensemble),
    forall x, x ∈ e1 /\ x ∈ e2 <-> x ∈ (e1 ∩ e2).
  
  Parameter union_spec :
    forall (e1 e2:Ensemble),
    forall x, x ∈ e1 \/ x ∈ e2 <-> x ∈ (e1 ∪ e2).

  Parameter difference_spec :
    forall (e1 e2:Ensemble),
    forall x, x ∈ e1 /\ ~(x ∈ e2) <-> x ∈ (e1 \ e2).

  Parameter subset_spec :
    forall (e1 e2:Ensemble), (e1 ⊂ e2) <-> (forall x, x ∈ e1 -> x ∈ e2).

  Parameter double_subset :
    forall (e1 e2:Ensemble), ((e1 ⊂ e2) /\ (e2 ⊂ e1)) <-> (e1 = e2).
  
  Parameter cartesian_spec :
    forall (e1 e2:Ensemble), forall x1 x2,
      (x1 ∈ e1 /\ x2 ∈ e2) <-> (x1, x2) ∈ (e1 × e2).

  Parameter powerset_spec :
    forall (e1 e2: Ensemble), e1 ⊆ e2 <-> e1 ∈ (powerset e2).

  (* TODO: Injection spec Bijection Spec etc *)
End AEnsembleFacts.


Module Type AExercices (Import M: AEnsemble) (Import M': AEnsembleFacts M).
  Module Aux.
    Include AEnsembleNotations M.
  End Aux.
  Import Aux.
  Local Open Scope ens_scope.
(*  
  Parameter exo1 : forall (A B C D: Ensemble), ((A ⊂ B) \/ (C ⊂ D)) -> (A ∩ C) ⊂ (B ∩ D).
  *)
  Theorem exo2 : forall (A B E F : Ensemble),
      A ⊂ E -> B ⊂ F ->
      ((E \ A) × (F \ B) = (E × F) \ ((A × F) ∪ (E × B))).
    intros A B E F hE hF.
    apply double_subset.
    split.
    * apply subset_spec.
      + intros T x hx.
        apply cartesian_spec in h

  
End AExercices.



  Parameter exo3 : 
  (* TODO: Formaliser exercice bijection entre nat et l'ensemble des parties finies de nat *)
  

  
    
End AEnsembleFacts.
