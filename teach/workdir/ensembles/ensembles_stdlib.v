Require Import Coq.Sets.Ensembles.

Context {U : Type}.
Arguments Ensemble {U}.
Arguments In {U}.
Arguments Included {U}.
Arguments Empty_set {U}.
Arguments Full_set {U}.
Arguments Union {U}.
Arguments Intersection {U}.

Lemma union_or (B C : Ensemble) (x : U) :
  In (Union B C) x <-> In B x \/ In C x.
Proof.
  split.
  - intros H. destruct H as [x H | x H].
    + now left.
    + now right.
  - intros [H | H].
    + now left.
    + now right.
Qed.
