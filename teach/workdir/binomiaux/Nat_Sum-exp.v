(**
This file is part of the Liber-Abaci library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** Support for iterated summations in N, the set natural numbers. *)

(* Strong induction is needed. *)
From Coq Require Import PeanoNat Wf_nat Compare_dec.

From LiberAbaci Require Import Nat_Compl.


Local Open Scope nat_scope.


Section Sum_Def.

(* Pas pour l'extended abstract.
(* Definition 1.
 Somme de 0 à n (n+1 termes).
 Sum from 0 to n (n+1 terms).
 sum_0_n n f = f n + f (n - 1) + ... + f 1 + f 0.
 n = 0 -> sum_0_n n f = 0. *)
Fixpoint sum_0_n (n : nat) (f : nat -> nat) : nat :=
  match n with
  | O => f 0
  | S n' => f (S n') + sum_0_n n' f
  end.

(* Somme de a à (a + n) (n+1 termes).
 Sum of n terms from a.
 sum_a_n a n f = f (a + n) + f (a + n - 1) + ... + f (a + 1) + f a.
 n = 0 -> sum_a_n a n f = 0. *)
Definition sum_a_n (a n : nat) (f : nat -> nat) : nat :=
  sum_0_n n (fun i => f (a + i)).

(*
Fixpoint sum_n (a n : nat) (f : nat -> nat) : nat :=
  match n with
  | O => f a / 0 / ??
  | S n' => f a + sum_n (S a) n' f
  end.
*)
*)

(* Pas pour l'extended abstract.
(* Somme de a inclus à b exclus.
 Sum from a included to b excluded.
 sum_range' a b f = f (b - 1) + ... + f (a + 1) + f a.
 b <= a -> sum_range' a b f = 0. *)
Definition sum_range' (a b : nat) (f : nat -> nat) : nat :=
  sum_n a (b - a) f.
*)

End Sum_Def.


(* À remettre si comparaison de sommes.
Section Sum_Def_alt.

Inductive sum_n_alt : nat -> nat -> (nat -> nat) -> nat -> Prop :=
| fsiO {a f} : sum_n_alt a 0 f 0
| fsiS {a f n s} : sum_n_alt a n f s -> sum_n_alt a (S n) f (s + f (a + n)).

Lemma sum_n_alt_correct f a n s : sum_n_alt a n f s <-> sum_n a n f = s.
Proof.
  split; intro h1.
  * induction h1.
    + reflexivity.
    + subst. reflexivity.
  * subst. induction n.
    + constructor.
    + simpl. constructor. apply IHn.
Qed.

End Sum_Def_alt.
*)


Section Sum_Facts.

(* LPO ou pas ?
Lemma sum_n_neq_0_rev a n f :
  sum_n a n f <> 0 -> exists i, a <= i < a + n /\ f i <> 0.
Proof.



Aglopted.

Lemma sum_n_neq_0_equiv a n f :
  sum_n a n f <> 0 <-> exists i, a <= i < a + n /\ f i <> 0.
Proof. split. apply sum_n_neq_0_rev. apply sum_n_neq_0. Qed.
*)

End Sum_Facts.


(* NOT FOR THE EXTENDED ABSTRACT!
Section Poly.

(* Limited support for polynomials on natural numbers. *)

Definition poly n a x := sum_n 0 (S n) (fun i => a i * x ^ i).

(* "_l" stands for the left argument of poly, ie "n". *)
Lemma poly_0_l a x : poly 0 a x = a 0.
Proof.
unfold poly. rewrite sum_n_1, Nat.pow_0_r, Nat.mul_1_r. reflexivity.
Qed.

(* "_r" stands for the right argument of poly, ie "x". *)
Lemma poly_0_r n a : poly n a 0 = a 0.
Proof.
unfold poly. rewrite <- (Nat.add_0_r (a 0)), sum_n_ind_l.
rewrite Nat.pow_0_r, Nat.mul_1_r. f_equal.
apply sum_n_eq_0. intros i [Hi _].
rewrite Nat.pow_0_l, Nat.mul_0_r. reflexivity.
apply Nat.neq_0_lt_0. rewrite <- Nat.le_succ_l. assumption.
Qed.

(* "_l" stands for the left argument of poly, ie "n". *)
Lemma poly_S_l n a x : poly (S n) a x = poly n a x + a (S n) * x ^ (S n).
Proof. unfold poly. rewrite sum_n_ind_r. reflexivity. Qed.

Lemma poly_ext n b a :
  (forall i, i <= n -> a i = b i) -> forall x, poly n a x = poly n b x.
Proof.
intros H x. unfold poly. apply sum_n_ext.
intros k [_ Hk]. rewrite Nat.add_0_l in Hk. rewrite H.
+ reflexivity.
+ apply Nat.lt_succ_r. assumption.
Qed.

Lemma poly_0 n a x : (forall i, i <= n -> a i = 0) -> poly n a x = 0.
Proof.
intros H. apply sum_n_eq_0. intros i [_ Hi]. rewrite H. reflexivity.
apply Nat.lt_succ_r; assumption.
Qed.

Lemma poly_0_rev n a x :
  poly n a x = 0 -> (x = 0 /\ a 0 = 0) \/ forall i, i <= n -> a i = 0.
Proof.
unfold poly. rewrite sum_n_eq_0_equiv. rewrite Nat.add_0_l. intros H.
destruct (nat2_eq_dec x 0 (a 0) 0) as [H1 | H1].
+ left; assumption.
+ right. intros i Hi. destruct x.
  - destruct H1 as [H1 | H1]. contradict H1; reflexivity.
    contradict H1. rewrite <- (Nat.mul_1_r (a 0)), <- (Nat.pow_0_r 0).
    apply H. split. apply Nat.le_refl. apply Nat.lt_0_succ.
  - apply (Nat.mul_eq_0_l _ (S x ^ i)).
    2: { apply Nat.pow_nonzero. discriminate. }
    apply H. split. apply Nat.le_0_l. apply Nat.lt_succ_r; assumption.
Qed.

Lemma poly_inj_alt n a b :
  (forall p x, p <= n -> poly p a x = poly p b x) ->
  forall i, i <= n -> a i = b i.
Proof.
induction n.
+ intros H i Hi. rewrite Nat.le_0_r in Hi. subst. specialize (H 0 0).
  rewrite !poly_0_l in H. apply H. apply Nat.le_refl.
+ intros H1.
  assert (H2 : forall i, i <= n -> a i = b i).
    apply IHn. intros p x Hp; apply H1. apply Nat.le_trans with n. assumption.
    apply Nat.le_succ_diag_r.
  intros i Hi1. destruct (le_dec i n) as [Hi2 | Hi2].
  - apply H2. assumption.
  - assert (Hi3 : i = S n).
      apply Nat.le_antisymm. assumption.
      apply Nat.le_succ_l. apply Nat.nle_gt in Hi2. assumption.
    subst. apply (Nat.add_cancel_l _ _ (poly n a 1)).
    rewrite H1 at 2. 2: apply Nat.le_succ_diag_r.
    replace (a (S n)) with (a (S n) * 1 ^ S n).
    2: { rewrite Nat.pow_1_l. rewrite Nat.mul_1_r. reflexivity. }
    replace (b (S n)) with (b (S n) * 1 ^ S n).
    2: { rewrite Nat.pow_1_l. rewrite Nat.mul_1_r. reflexivity. }
    rewrite <- !poly_S_l.
    apply H1. assumption.
Qed.

Lemma poly_inj n a b :
  (forall x, poly n a x = poly n b x) -> forall i, i <= n -> a i = b i.
Proof.
(*
intros H. apply poly_inj_alt. revert a b H.
apply (lt_wf_ind n
    (fun n => forall a b, (forall x, poly n a x = poly n b x) ->
      forall p x : nat, p <= n -> poly p a x = poly p b x)). clear n.
intros n IHn a b H p x Hp.



revert a b; induction n.
+ intros a b H i Hi. apply Nat.le_0_r in Hi. subst.
  rewrite <- (poly_0_l a 0), <- (poly_0_l b 0). apply H.
+ unfold poly in *; simpl in *.
intros a b H i Hi1.
generalize (IHn a b).


 destruct (le_lt_dec i n) as [Hi2 | Hi2].
  - 
*)








Aglopted.

Lemma poly_split m n a x :
  m <= n ->
  poly n a x = poly m a x + sum_n (S m) (n - m) (fun i => a i * x ^ i).
Proof.
rewrite Nat.succ_le_mono. rewrite <- Nat.sub_succ. apply sum_n_split.
Qed.

Lemma poly_add m n a b x :
  (forall i, m < i -> a i = 0) ->
  (forall j, n < j -> b j = 0) ->
  let c k := a k + b k in
  poly m a x + poly n b x = poly (max m n) c x.
Proof.
set (P m n := forall a b x,
  (forall i, m < i -> a i = 0) ->
  (forall j, n < j -> b j = 0) ->
  let c := fun k => a k + b k in
  poly m a x + poly n b x = poly (max m n) c x).
revert m n a b x. apply (nat2_ind_sym P).
+ intros m n HP a b x Ha Hb c.
  rewrite Nat.add_comm. rewrite HP. 2,3: assumption.
  rewrite Nat.max_comm. apply poly_ext.
  intros i Hi. unfold c. apply Nat.add_comm.
+ unfold P. intros a b x Ha Hb. rewrite Nat.max_id.
  rewrite !poly_0_l. reflexivity.
+ intros m n HP a b x Ha Hb c. rewrite poly_S_l.
  rewrite <- Nat.add_assoc, (Nat.add_comm (a _ * _)), Nat.add_assoc.
  set (a' i := if Nat.eq_dec i (S m) then 0 else a i). rewrite (poly_ext m a').
  2: { intros i Hi; unfold a'. destruct (Nat.eq_dec i (S m)). 2: reflexivity.
    subst. contradict Hi. apply Nat.nle_gt, Nat.lt_succ_diag_r. }
  rewrite HP. 3: assumption.
  2: { intros i Hi; unfold a'. destruct (Nat.eq_dec i (S m)). reflexivity.
    apply Ha. apply nat_lt_S_wnf; assumption. }
  unfold c. destruct (le_lt_dec n m) as [H | H].
  - assert (Hb1 : b (S m) = 0). apply Hb. apply Nat.lt_succ_r. assumption.
    rewrite !Nat.max_l. 3: assumption.
    2: { apply Nat.le_trans with m. assumption. apply Nat.le_succ_diag_r. }
    rewrite poly_S_l. rewrite Hb1, Nat.add_0_r. f_equal. apply poly_ext.
    intros i Hi; unfold a'. destruct (Nat.eq_dec i (S m)). 2: reflexivity.
    subst; contradict Hi. apply Nat.nle_gt, Nat.lt_succ_diag_r.
  - assert (H1 : m <= n). apply Nat.lt_le_incl. assumption.
    apply Nat.le_succ_l in H. rewrite !Nat.max_r. 2,3: assumption.
    rewrite 2!(poly_split m n). 2,3: assumption.
    rewrite <- Nat.add_assoc. f_equal.
    * apply poly_ext. intros i Hi; unfold a'. destruct (Nat.eq_dec i (S m)).
      2: reflexivity.
      subst. contradict Hi. apply Nat.nle_gt, Nat.lt_succ_diag_r.
    * replace (n - m) with (S (pred (n - m))).
      2: { apply Nat.succ_pred, Nat.sub_gt. assumption. }
      rewrite Nat.add_comm. rewrite sum_n_ind_l.
      replace (a' (S m)) with 0.
      2: { unfold a'. destruct (Nat.eq_dec _ _) as [_ | H2]. reflexivity.
        contradict H2. reflexivity. }
      rewrite Nat.add_0_l, Nat.add_assoc. rewrite <- Nat.mul_add_distr_r.
      rewrite sum_n_ext with (f2 := fun i => (a i + b i) * x ^ i).
      2: { intros k [Hk _]. unfold a'. destruct (Nat.eq_dec _ _). 2: reflexivity.
        subst. contradict Hk. apply Nat.nle_gt, Nat.lt_succ_diag_r. }
      rewrite <- (sum_n_ind_l _ _ (fun i => (a i + b i) * x ^ i)). reflexivity.
Qed.

Lemma poly_mul m n a b x :
  (forall i, m < i -> a i = 0) ->
  (forall j, n < j -> b j = 0) ->
  let c k := sum_n 0 (S k) (fun l => a l * b (k - l)) in
  poly m a x * poly n b x = poly (m + n) c x.
Aglopted.





(*Lemma sum_n_poly_mul_n_m n m a b x: 
  (forall i, i > n -> a i = 0) -> 
  (forall j, j > m -> b j = 0) -> 
  sum_n 0 (S n) (fun i => a i * x^i) * sum_n 0 (S m) (fun j => b j * x^j) =
    sum_n 0 (S (n + m)) (fun k => sum_n 0 (S k) (fun j => a j * b (k - j) * x^k)).*)


End Poly.
*)
