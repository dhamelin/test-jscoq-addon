(**
This file is part of the Liber-Abaci library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** Support for iterated summations in N, the set natural numbers. *)

(* Strong induction is needed. *)
From Coq Require Import PeanoNat Wf_nat Compare_dec.

From LiberAbaci Require Import Nat_Compl.


Local Open Scope nat_scope.


Section Sum_Def.

(* Somme de n termes à partir de a. / Sum of n terms from a.
 sum_n a n f = f a + f (a + 1) + ... + f (a + n - 1).
 n = 0 -> sum_n a n f = 0. *)
Fixpoint sum_n (a n : nat) (f : nat -> nat) : nat :=
  match n with
  | O => 0
  | S n' => sum_n a n' f + f (a + n')
  end.

(* Somme de a à b inclus. / Sum from a to b included.
 sum_range a b f = f a + f (a + 1) + ... + f (b - 1) + f b.
 b < a -> sum_range a b f = 0. *)
Definition sum_range (a b : nat) (f : nat -> nat) : nat :=
  if le_lt_dec a b then sum_n a (b - a + 1) f else 0.

Lemma sum_range_correct_l a b f :
  a <= b -> sum_range a b f = sum_n a (b - a + 1) f.
Proof.
intros H; unfold sum_range. destruct (le_lt_dec _ _). reflexivity.
contradict H. apply Nat.nle_gt; assumption.
Qed.

Lemma sum_range_correct_r a b f : b < a -> sum_range a b f = 0.
Proof.
intros H; unfold sum_range. destruct (le_lt_dec _ _). 2: reflexivity.
contradict H. apply Nat.nlt_ge; assumption.
Qed.

Lemma sum_n_eq a n f : 0 < a + n -> sum_n a n f = sum_range a (a + n - 1) f.
Proof.
intros H. destruct (Nat.eq_dec n 0) as [Hn | Hn].
+ subst. rewrite sum_range_correct_r. reflexivity.
  rewrite Nat.add_0_r in *. rewrite Nat.sub_1_r.
  apply Nat.lt_pred_l. apply Nat.neq_0_lt_0. assumption.
+ assert (Hn1 : 1 <= n). apply Nat.le_succ_l, Nat.neq_0_lt_0. assumption.
  assert (H1 : a <= a + n - 1).
    rewrite <- Nat.add_sub_assoc. apply Nat.le_add_r. assumption.
  rewrite sum_range_correct_l. 2: assumption.
  replace (_ + 1) with n. reflexivity.
  rewrite <- Nat.add_sub_swap. 2: assumption.
  rewrite <- (Nat.add_sub_assoc a). 2: assumption.
  rewrite <- Nat.add_assoc. rewrite Nat.sub_add. 2: assumption.
  rewrite Nat.add_comm. rewrite Nat.add_sub. reflexivity.
Qed.

Lemma sum_range_0_l n f : sum_range 0 n f = sum_n 0 (S n) f.
Proof.
rewrite sum_range_correct_l. 2: apply Nat.le_0_l.
rewrite Nat.sub_0_r, Nat.add_1_r. reflexivity.
Qed.

End Sum_Def.


Section Sum_Facts.

Lemma sum_n_ext a n f1 f2 :
  (forall k, a <= k < a + n -> f1 k = f2 k) -> sum_n a n f1 = sum_n a n f2.
Proof.
intro H. induction n. reflexivity.
simpl. rewrite IHn.
+ rewrite H. reflexivity. apply nat_le_add_r_lt_S.
+ intros k Hk. apply H. apply nat_le_lt_add_S. assumption.
Qed.

Lemma sum_range_ext a b f1 f2 :
  (forall k, a <= k <= b -> f1 k = f2 k) -> sum_range a b f1 = sum_range a b f2.
Proof.
destruct (le_lt_dec a b) as [H | H].
+ intros Hf. rewrite !sum_range_correct_l. 2,3: assumption.
  apply sum_n_ext. intros k [Hk1 Hk2]. apply Hf. split. assumption.
  revert Hk2. rewrite Nat.add_1_r. rewrite Nat.add_succ_r, Nat.lt_succ_r.
  rewrite Nat.add_comm. rewrite Nat.sub_add. easy. assumption.
+ intros _. rewrite !sum_range_correct_r. reflexivity. all: assumption.
Qed.

Lemma sum_n_0 a f : sum_n a 0 f = 0.
Proof. reflexivity. Qed.

Lemma sum_n_1 a f : sum_n a 1 f = f a.
Proof. unfold sum_n. now rewrite !Nat.add_0_r. Qed.

(* (S n) est le successeur de n, c'est-à-dire (n + 1). /
 (S n) is the successor of n, ie (n + 1). *)
Lemma sum_n_ind_r a n f : sum_n a (S n) f = sum_n a n f + f (a + n).
Proof. reflexivity. Qed.

Lemma sum_n_ind_l a n f : sum_n a (S n) f = f a + sum_n (S a) n f.
Proof.
revert a. 
apply (lt_wf_ind n).
clear n. 
intros n IHn a. rewrite sum_n_ind_r. destruct n.
+ rewrite !Nat.add_0_r. reflexivity.
+ rewrite IHn. 2: apply Nat.lt_succ_diag_r.
  rewrite sum_n_ind_r. rewrite Nat.add_succ_comm, Nat.add_assoc. reflexivity.
Qed.

Lemma sum_n_ind (P : nat -> Prop) a n f :
  P 0 -> (forall i j, P i -> P j -> P (i + j)) ->
  (forall i, a <= i < a + n -> P (f i)) ->
  P (sum_n a n f).
Proof.
intros HP0 HP Hf. induction n.
+ rewrite sum_n_0. assumption.
+ rewrite sum_n_ind_r. apply HP.
  - apply IHn. intros i Hi. apply Hf. apply nat_le_lt_add_S. assumption.
  - apply Hf. apply nat_le_add_r_lt_S.
Qed.

Lemma sum_n_concat a n1 n2 f :
    sum_n a n1 f + sum_n (a + n1) n2 f = sum_n a (n1 + n2) f.
Proof.
revert a n2 f; induction n1.
+ intros. rewrite sum_n_0, Nat.add_0_l. f_equal. easy.
+ intros a n2 f. rewrite Nat.add_succ_comm. rewrite <- IHn1.
  rewrite sum_n_ind_r. rewrite sum_n_ind_l. rewrite Nat.add_succ_r.
  apply eq_sym, Nat.add_assoc.
Qed.

Lemma sum_n_split m n a f :
  m <= n -> sum_n a n f = sum_n a m f + sum_n (a + m) (n - m) f.
Proof.
intros H. rewrite sum_n_concat. rewrite Nat.add_comm. rewrite Nat.sub_add.
reflexivity. assumption.
Qed.

Lemma sum_n_extract i a n f :
  i < n -> sum_n a n f = sum_n a i f + f (a + i) + sum_n (a + S i) (n - S i) f.
Proof.
intros Hi. rewrite (sum_n_split i). 2: apply Nat.lt_le_incl; assumption.
rewrite <- Nat.add_assoc. f_equal.
rewrite Nat.add_succ_r. rewrite <- sum_n_ind_l. f_equal.
rewrite Nat.sub_succ_r. rewrite Nat.succ_pred. reflexivity.
apply Nat.sub_gt. assumption.
Qed.

Lemma sum_n_le a n f g :
  (forall i, a <= i < a + n -> f i <= g i) ->
  sum_n a n f <= sum_n a n g.
Proof.
intros H. induction n.
+ rewrite !sum_n_0. apply Nat.le_refl.
+ rewrite !sum_n_ind_r. apply Nat.add_le_mono.
  2: { apply H. apply nat_le_add_r_lt_S. }
  apply IHn. intros i Hi. apply H. apply nat_le_lt_add_S. assumption.
Qed.

Lemma sum_n_lt a n f g :
  (forall i, a <= i < a + n -> f i <= g i) ->
  (exists i, a <= i < a + n /\ f i < g i) ->
  sum_n a n f < sum_n a n g.
Proof.
revert a f g. induction n; intros a f g H1 [i [Hi H2]].
+ destruct Hi as [Hi1 Hi2]. contradict Hi2.
  rewrite Nat.add_0_r. apply Nat.nlt_ge. assumption.
+ rewrite (sum_n_extract (i - a) _ _ f), (sum_n_extract (i - a) _ _ g).
  2,3: apply nat_le_lt_add_sub; assumption.
  destruct Hi as [Hi1 Hi2]. apply Nat.add_lt_le_mono.
  2: { apply sum_n_le. intros j [Hj1 Hj2]. apply H1. split.
    + apply Nat.le_trans with (a + S (i - a)). apply Nat.le_add_r. assumption.
    + apply Nat.lt_le_trans with (a + S (i - a) + (S n - S (i - a))). assumption.
      rewrite Nat.sub_succ. rewrite <- Nat.add_assoc. rewrite Nat.add_succ_l.
      rewrite (Nat.add_comm (i - a)). rewrite Nat.sub_add. apply Nat.le_refl.
      apply Nat.le_sub_le_add_l. apply nat_lt_add_succ_r. assumption. }
  apply Nat.add_le_lt_mono.
  2: { replace (a + (i - a)) with i. assumption.
    rewrite Nat.add_comm. rewrite Nat.sub_add. reflexivity. assumption. }
  apply sum_n_le. intros j [Hj1 Hj2]. apply H1. split. assumption.
  apply Nat.lt_trans with i. 2: assumption.
  rewrite <- (Nat.sub_add a i). 2: assumption.
  rewrite Nat.add_comm. assumption.
Qed.

Lemma sum_n_eq_0 a n f :
  (forall i, a <= i < a + n -> f i = 0) -> sum_n a n f = 0.
Proof.
intros H. apply trans_eq with (sum_n a n (fun _ => 0)).
+ apply sum_n_ext. assumption.
+ clear; revert a; induction n. reflexivity.
  intros a; rewrite sum_n_ind_l. rewrite IHn. reflexivity.
Qed.

Lemma sum_range_eq_0 a b f :
  (forall i, a <= i <= b -> f i = 0) -> sum_range a b f = 0.
Proof.
intros Hf. destruct (le_lt_dec a b) as [H | H].
2: { apply sum_range_correct_r. assumption. }
rewrite sum_range_correct_l. 2: assumption.
apply sum_n_eq_0. intros i [Hi1 Hi2]. apply Hf. split. assumption.
apply Nat.lt_succ_r. replace (S b) with (a + (b - a + 1)). assumption.
rewrite Nat.add_1_r. rewrite <- Nat.sub_succ_l. 2: assumption.
rewrite Nat.add_comm. rewrite Nat.sub_add. reflexivity.
apply Nat.le_trans with b. assumption. apply Nat.le_succ_diag_r.
Qed.

Lemma sum_n_eq_0_rev a n f :
  sum_n a n f = 0 -> forall i, a <= i < a + n -> f i = 0.
Proof.
induction n.
+ rewrite Nat.add_0_r. intros _ i [Hi1 Hi2].
  contradict Hi1. apply Nat.nle_gt. assumption.
+ rewrite sum_n_ind_r. rewrite Nat.eq_add_0.
  intros [H1 H2] i Hi1. destruct (Nat.eq_dec i (a + n)) as [Hi2 | Hi2].
  - subst. assumption.
  - apply IHn. assumption.
    destruct Hi1 as [Hi1a Hi1b]. split. assumption.
    apply nat_lt_S_wnl. 2: assumption.
    rewrite <- Nat.add_succ_r. assumption.
Qed.

Lemma sum_n_eq_0_equiv a n f :
  sum_n a n f = 0 <-> forall i, a <= i < a + n -> f i = 0.
Proof. split. apply sum_n_eq_0_rev. apply sum_n_eq_0. Qed.

Lemma sum_n_neq_0 a n f :
  (exists i, a <= i < a + n /\ f i <> 0) -> sum_n a n f <> 0.
Proof.
intros [i [Hi Hf]]. contradict Hf.
apply (sum_n_eq_0_rev a n). all: assumption.
Qed.

Lemma sum_n_add a n f g :
  sum_n a n f + sum_n a n g = sum_n a n (fun i => f i + g i).
Proof.
induction n. reflexivity.
rewrite !sum_n_ind_r. rewrite <- IHn. rewrite 2!Nat.add_assoc. f_equal.
rewrite <- Nat.add_assoc. rewrite (Nat.add_comm (f _)). apply Nat.add_assoc.
Qed.

Lemma sum_n_sub a n f g :
  (forall i, a <= i < a + n -> g i <= f i) ->
  sum_n a n f - sum_n a n g = sum_n a n (fun i => f i - g i).
Proof.
intros H. induction n. reflexivity.
rewrite !sum_n_ind_r. rewrite <- IHn.
2: { intros i Hi. apply H. apply nat_le_lt_add_S. assumption. }
rewrite Nat.sub_add_distr. rewrite Nat.add_sub_assoc.
2: { apply H. apply nat_le_add_r_lt_S. }
rewrite Nat.add_sub_swap.
2: { apply sum_n_le. intros i Hi. apply H. apply nat_le_lt_add_S. assumption. }
reflexivity.
Qed.

Lemma sum_n_mul_add_distr_l a n f c :
  c * sum_n a n f = sum_n a n (fun i => c * f i).
Proof.
induction n. apply Nat.mul_0_r.
rewrite !sum_n_ind_r. rewrite <- IHn. apply Nat.mul_add_distr_l.
Qed.

Lemma sum_n_mul_add_distr_r a n f c :
  sum_n a n f * c = sum_n a n (fun i => f i * c).
Proof.
rewrite Nat.mul_comm. rewrite sum_n_mul_add_distr_l.
apply sum_n_ext. intros; apply Nat.mul_comm.
Qed.

Lemma sum_n_shift_l a n f t :
  t <= a -> sum_n a n f = sum_n (a - t) n (fun i => f (i + t)).
Proof.
intros Ht. induction n. reflexivity.
rewrite !sum_n_ind_r. rewrite IHn.
replace (a - t + n + t) with (a + n). reflexivity.
rewrite (Nat.add_comm (a - t)). rewrite <- Nat.add_assoc.
rewrite Nat.sub_add. 2: assumption.
apply Nat.add_comm.
Qed.

Lemma sum_n_shift_r a n f t :
  sum_n a n f = sum_n (a + t) n (fun i => f (i - t)).
Proof.
rewrite (sum_n_shift_l (a + t) _ _ t). 2: apply Nat.le_add_l.
rewrite Nat.add_sub. apply sum_n_ext.
intros. rewrite Nat.add_sub. reflexivity.
Qed.

Lemma sum_n_reverse n f : sum_n 0 (S n) f = sum_n 0 (S n) (fun i => f (n - i)).
Proof.
induction n. easy.
rewrite sum_n_ind_r. rewrite sum_n_ind_l with (n := S n).
rewrite sum_n_shift_l with (t := 1) (a := 1). 2: apply Nat.le_refl.
rewrite Nat.add_0_l. rewrite Nat.sub_0_r. rewrite Nat.sub_diag.
rewrite Nat.add_comm. f_equal. rewrite IHn. apply sum_n_ext.
intros. rewrite Nat.add_1_r. rewrite Nat.sub_succ. reflexivity.
Qed.

End Sum_Facts.
