(**
This file is part of the Liber-Abaci library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
 *)

(** Useful exports for the provided worksheet.
 This includes the lists of results on natural numbers from Arith.PeanoNat.Nat
 that are used in modules Nat_Sum, Nat_Factorial and Nat_Binomial.

 Usage: tune exports and definitions to your needs, and import the resulting
 module in the worksheet. *)

From Coq Require Export Arith ZArith_base Lia.

(* Helper for results on natural numbers used in Nat_Sum. *)
Export Nat (eq_add_0, eq_dec).
Export Nat (le_0_l, le_refl, le_succ_diag_r, le_succ_l, le_trans, lt_pred_l,
            lt_succ_diag_r, lt_succ_r, neq_0_lt_0, nle_gt, nlt_ge).
Export Nat (add_0_l, add_0_r, add_1_r, add_assoc, add_comm, add_sub,
            add_sub_assoc, add_sub_swap, add_succ_comm, add_succ_r, sub_0_r,
            sub_1_r, sub_add, sub_add_distr, sub_diag, sub_succ, sub_succ_l).
Export Nat (add_le_mono, le_add_l, le_add_r).
Export Nat (mul_0_r, mul_add_distr_l, mul_comm).

(* Helper for results on natural numbers used in Nat_Factorial. *)
Export Nat (eq_dec, succ_pred).
Export Nat (le_0_l, le_0_r, le_1_r, le_antisymm, le_lt_trans, le_refl,
            le_succ_l, le_trans, nle_gt, nle_succ_0).
Export Nat (lt_0_1, lt_0_succ, lt_le_trans, lt_neq, lt_succ_diag_r, lt_trans,
            neq_0_lt_0, nlt_ge).
Export Nat (add_0_r).
Export Nat (mul_1_r, mul_assoc, mul_comm, mul_eq_0, mul_pos_pos, neq_mul_0).
Export Nat (div_mul, divide_factor_r, divide_mul_l, mod_divides).

(* Helper for results on natural numbers used in Nat_Binomial. *)
Export Nat (eq_dec, pred_succ, succ_pred).
Export Nat (le_0_l, le_0_r, le_antisymm, le_refl, le_trans, le_succ_diag_r,
            le_succ_le_pred, le_trans).
Export Nat (lt_0_1, lt_0_succ, lt_1_r, lt_le_incl, lt_neq, lt_succ_diag_r,
            lt_succ_lt_pred, lt_succ_r, lt_trans, neq_0_lt_0, nle_gt, nlt_ge,
            succ_le_mono, succ_lt_mono).
Export Nat (add_0_l, add_0_r, add_1_l, add_1_r, add_assoc, add_comm, add_sub,
            add_sub_swap, add_succ_comm, add_succ_r, sub_0_l, sub_0_le,
            sub_0_r, sub_1_r, sub_add, sub_diag, sub_succ_l, sub_succ_r).
Export Nat (add_lt_mono_r, add_pos_l, le_add_l, lt_add_pos_l, lt_lt_add_l).
Export Nat (mul_0_l, mul_0_r, mul_1_l, mul_1_r, mul_assoc, mul_cancel_l,
            mul_comm, mul_succ_l).
Export Nat (double_twice, mul_add_distr_l, mul_add_distr_r).
Export Nat (div_mul, div_small, mod_divides).
Export Nat (pow_0_r, pow_1_l, pow_2_r, pow_succ_r').

From LiberAbaci Require Export Nat_Compl Nat_Sum Nat_Factorial Nat_Binomial.

Definition formule_du_pion := binom_committee_chair.
Definition committee_chair_identity := binom_committee_chair.
Definition formule_de_vandermonde := binom_vandermonde.
Definition vandermonde_identity := binom_vandermonde.

(* Helper for results on natural numbers used in exercises. *)
Export Nat (lt_1_2, sub_le_mono_r).
Export Nat (div_mul_cancel_l, divide_div_mul_exact, mod_divide).
