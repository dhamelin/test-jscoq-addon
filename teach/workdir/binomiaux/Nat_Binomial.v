(**
This file is part of the Liber-Abaci library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** Support for binomial coefficients in N, the set of natural numbers. *)

From Coq Require Import PeanoNat Compare_dec.

From LiberAbaci Require Import Nat_Compl Nat_Sum Nat_Factorial.


Local Open Scope nat_scope.


Section Binom_Def.

Fixpoint binom (n k : nat) : nat :=
  match n, k with
  | _, O => 1
  | O, _ => 0
  | S n', S k' => binom n' k' + binom n' k
  end.

Arguments binom : simpl nomatch.

Lemma binom_0_r n : binom n 0 = 1.
Proof. destruct n; reflexivity. Qed.

Lemma binom_0_l k : k <> 0 -> binom 0 k = 0.
Proof. destruct k; intros Hk. contradict Hk. all: reflexivity. Qed.

(* Formule de Pascal. / Pascal's rule. *)
Lemma binom_pascal n k : binom n k + binom n (S k) = binom (S n) (S k).
Proof. reflexivity. Qed.

End Binom_Def.


Section Binom_Facts.

Lemma binom_eq_0 n k : n < k -> binom n k = 0.
Proof.
revert k; induction n; intros k H.
+ apply binom_0_l. apply Nat.neq_0_lt_0. assumption.
+ destruct k.
  - contradict H. apply Nat.nlt_ge, Nat.le_0_l.
  - rewrite <- binom_pascal, !IHn. reflexivity.
    apply Nat.lt_trans with (S n). apply Nat.lt_succ_diag_r. assumption.
    apply Nat.succ_lt_mono. assumption.
Qed.

Lemma binom_S_l n k : k <> 0 -> binom (S n) k = binom n (pred k) + binom n k.
Proof.
intros Hk. rewrite <- (Nat.succ_pred k). 2: assumption.
apply eq_sym, binom_pascal.
Qed.

Lemma binom_S_r n k :
  (n <> 0 \/ k <> 0) ->
  binom n (S k) = binom (pred n) k + binom (pred n) (S k).
Proof.
destruct n.
+ intros [Hn | Hk]. contradict Hn; reflexivity.
  rewrite !binom_eq_0. reflexivity.
  all: apply Nat.neq_0_lt_0. 2: assumption. all: discriminate.
+ intros _. rewrite Nat.pred_succ. apply eq_sym, binom_pascal.
Qed.

Lemma binom_diag n : binom n n = 1.
Proof.
induction n. apply binom_0_r.
rewrite <- binom_pascal, IHn, binom_eq_0. reflexivity.
apply Nat.lt_succ_diag_r.
Qed.

Lemma binom_1_r n : binom n 1 = n.
Proof.
induction n. reflexivity.
rewrite <- binom_pascal, binom_0_r, IHn. apply Nat.add_1_l.
Qed.

Lemma binom_ge_0 n k : 0 <= binom n k.
Proof. apply Nat.le_0_l. Qed.

Lemma binom_gt_0 n k : k <= n -> 0 < binom n k.
Proof.
revert k. induction n; intros k H.
+ apply Nat.le_0_r in H. subst. apply Nat.lt_0_1.
+ destruct k. rewrite binom_0_r; apply Nat.lt_0_1.
  rewrite binom_S_l. 2: discriminate.
  apply Nat.add_pos_l. apply IHn. apply Nat.succ_le_mono. assumption.
Qed.

Lemma binom_neq_0 n k : k <= n -> binom n k <> 0.
Proof. intros. apply Nat.neq_0_lt_0, binom_gt_0. assumption. Qed.

Lemma binom_eq_0_rev n k : binom n k = 0 -> n < k.
Proof.
intros H. apply Nat.nle_gt. contradict H. apply binom_neq_0. assumption.
Qed.

Lemma binom_eq_0_equiv n k : binom n k = 0 <-> n < k.
Proof. split. apply binom_eq_0_rev. apply binom_eq_0. Qed.

Lemma binom_neq_0_rev n k : binom n k <> 0 -> k <= n.
Proof.
intros H. apply Nat.nlt_ge. contradict H. apply binom_eq_0. assumption.
Qed.

Lemma binom_neq_0_equiv n k : binom n k <> 0 <-> k <= n.
Proof. split. apply binom_neq_0_rev. apply binom_neq_0. Qed.

Lemma binom_le_S n k : binom n k <= binom (S n) k.
Proof.
destruct k. rewrite binom_0_r; apply Nat.le_refl. apply Nat.le_add_l.
Qed.

Lemma binom_le_pred n k : binom (pred n) k <= binom n k.
Proof. destruct n. apply Nat.le_refl. apply binom_le_S. Qed.

Lemma binom_le n1 n2 k : n1 <= n2 -> binom n1 k <= binom n2 k.
Proof.
revert n1 k. induction n2; intros n1 k Hn.
+ apply Nat.le_0_r in Hn. subst. apply Nat.le_refl.
+ destruct (Nat.eq_dec n1 (S n2)) as [Hn1 | Hn1].
  - subst. apply Nat.le_refl.
  - apply Nat.le_trans with (binom n2 k). 2: apply binom_le_S.
    apply IHn2. apply nat_le_S_wnl; assumption.
Qed.

(* Version de binom_le avec hypothèse optimale. /
 Version of binom_le with optimal hypothesis. *)
Lemma binom_le_alt n1 n2 k :
  (k = 0 \/ n1 < k \/ n1 <= n2) -> binom n1 k <= binom n2 k.
Proof.
intros [H | [H | H]].
+ subst. rewrite !binom_0_r. apply Nat.le_refl.
+ rewrite binom_eq_0. apply Nat.le_0_l. assumption.
+ apply binom_le. assumption.
Qed.

Lemma binom_lt_S n k : 0 < k <= S n -> binom n k < binom (S n) k.
Proof.
intros [Hk H]. rewrite binom_S_l. 2: apply Nat.neq_0_lt_0; assumption.
apply Nat.lt_add_pos_l. apply binom_gt_0. apply Nat.succ_le_mono.
rewrite Nat.succ_pred. 2: apply Nat.neq_0_lt_0. all: assumption.
Qed.

Lemma binom_lt_pred n k : 0 < k <= n -> binom (pred n) k < binom n k.
Proof.
destruct n.
+ intros [H1 H2]. contradict H1. apply Nat.nlt_ge. assumption.
+ rewrite Nat.pred_succ. apply binom_lt_S.
Qed.

Lemma binom_lt n1 n2 k : 0 < k <= n2 -> n1 < n2 -> binom n1 k < binom n2 k.
Proof.
revert n1 k. induction n2; intros n1 k Hk H1.
+ contradict H1. apply Nat.nlt_ge, Nat.le_0_l.
+ destruct (Nat.eq_dec n1 n2) as [H2 | H2].
  - subst. apply binom_lt_S. assumption.
  - destruct (Nat.eq_dec k (S n2)) as [Hk2 | Hk2].
    * subst. rewrite binom_eq_0. 2: assumption.
      rewrite binom_diag. apply Nat.lt_0_1.
    * apply Nat.lt_trans with (binom n2 k).
      ++ apply IHn2. destruct Hk as [Hk0 Hk1]. split. assumption.
        apply nat_le_S_wnl; assumption.
        apply nat_lt_S_wnl; assumption.
      ++ apply binom_lt_S. assumption.
Qed.

Lemma binom_gt_1 n k : 0 < k < n -> 1 < binom n k.
Proof.
intros [Hk H]. rewrite <- (binom_diag k). apply binom_lt. 2: assumption.
split. assumption. apply Nat.lt_le_incl. assumption.
Qed.

Lemma binom_gt_1_contra n k : binom n k <= 1 -> k <= 0 \/ n <= k.
Proof.
destruct k; rewrite <- Nat.nlt_ge; intros H. left; apply Nat.le_refl.
right. apply Nat.nlt_ge. contradict H. apply binom_gt_1. split. 2: assumption.
apply Nat.lt_0_succ.
Qed.

Lemma binom_eq_1_equiv n k : binom n k = 1 <-> k = 0 \/ k = n.
Proof.
apply iff_sym; split.
+ intros [H | H]; subst. apply binom_0_r. apply binom_diag.
+ rewrite nat_eq_1_equiv. intros [H1 H2].
  apply binom_gt_1_contra in H2. destruct H2 as [H2 | H2].
  - left. apply Nat.le_0_r. assumption.
  - right. apply Nat.le_antisymm. apply binom_neq_0_rev. all: assumption.
Qed.

(* Formule du pion. / Committee-chair identity. *)
Lemma binom_committee_chair n k :
  S k * binom n (S k) = n * binom (pred n) k.
Proof.
revert k. induction n. intros; rewrite Nat.mul_0_r; reflexivity.
intros k. destruct k.
+ rewrite binom_0_r, binom_1_r. apply Nat.mul_comm.
+ rewrite Nat.pred_succ. destruct n.
  - rewrite !binom_eq_0. rewrite Nat.mul_0_r; reflexivity.
    apply Nat.lt_0_succ. apply nat_S2_gt_1.
  - apply eq_sym. rewrite Nat.mul_succ_l. rewrite <- binom_pascal at 1.
    rewrite Nat.mul_add_distr_l. rewrite <- !IHn.
    rewrite (Nat.add_comm ((S _) * _)). rewrite <- Nat.add_assoc.
    rewrite <- Nat.mul_succ_l. rewrite <- Nat.mul_add_distr_l. f_equal.
    rewrite Nat.add_comm. apply binom_pascal.
Qed.

(* "_w0" means "when (something is) 0", here "when n - i = 0". *)
Lemma binom_committee_chair_iter_w0 n k i :
  n <= i -> (k <> 0 \/ n = i) ->
  fact (k + i) * fact (n - i) * binom n (k + i) =
    fact n * fact k * binom (n - i) k.
Proof.
intros Hn1. rewrite nat_sub_0_le. 2: assumption.
intros [Hk | Hn].
+ rewrite !binom_eq_0. rewrite !Nat.mul_0_r; reflexivity.
  apply Nat.neq_0_lt_0. assumption.
  apply nat_lt_add_l_wn0; assumption.
+ clear Hn1; subst. destruct k.
  - rewrite !binom_diag. reflexivity.
  - rewrite !binom_eq_0. rewrite !Nat.mul_0_r; reflexivity.
    2: apply Nat.lt_add_pos_l. all: apply Nat.lt_0_succ.
Qed.

(* Formule du pion itérée. / Iterated committee-chair identity. *)
Lemma binom_committee_chair_iter n k i :
  (k <> 0 \/ i <= n) ->
  fact (k + i) * fact (n - i) * binom n (k + i) =
    fact n * fact k * binom (n - i) k.
Proof.
destruct (le_lt_dec n i) as [H1 | H1].
+ intros H2. apply binom_committee_chair_iter_w0. assumption.
  destruct H2. left; assumption. right; apply Nat.le_antisymm; assumption.
+ revert n k H1. induction i; intros n k H1 H2.
  - rewrite Nat.add_0_r, Nat.sub_0_r. f_equal. apply Nat.mul_comm.
  - rewrite Nat.add_succ_r, fact_S, Nat.sub_succ_r, nat_pred_sub.
    replace (_ * _ * _ * _) with
        (n * (fact (k + i) * fact (pred n - i) * binom (pred n) (k + i))).
    2: { rewrite Nat.mul_comm, <- Nat.mul_assoc, (Nat.mul_comm _ n).
      rewrite <- binom_committee_chair, Nat.mul_assoc. f_equal.
      rewrite <- Nat.mul_assoc, (Nat.mul_comm (fact (pred _ - _))).
      apply Nat.mul_assoc. }
    rewrite IHi.
    2: { apply Nat.lt_succ_lt_pred. assumption. }
    2: { destruct H2 as [H2 | H2]. left; assumption.
      right; apply Nat.le_succ_le_pred. assumption. }
    rewrite 2!Nat.mul_assoc.
    rewrite <- (Nat.succ_pred n) at 1 4.
    2: { contradict H1. subst. apply Nat.nlt_ge. apply Nat.le_0_l. }
    rewrite (Nat.mul_comm (S _)), <- fact_S. reflexivity.
Qed.

Lemma binom_fact n k : k <= n -> binom n k * (fact k * fact (n - k)) = fact n.
Proof.
revert k. induction n.
+ intro. rewrite Nat.le_0_r. intros; subst. reflexivity.
+ intros k Hk. destruct k. rewrite !Nat.mul_1_l; reflexivity.
  rewrite !fact_S. rewrite (Nat.mul_comm (fact _)), !Nat.mul_assoc.
  rewrite (Nat.mul_comm (binom _ _)), binom_committee_chair, Nat.pred_succ.
  rewrite <- !Nat.mul_assoc, Nat.mul_comm. rewrite IHn. reflexivity.
  apply Nat.succ_le_mono. assumption.
Qed.

(* "_w0" means "when (something is) 0", here "when binom n k = 0". *)
Lemma binom_fact_div_w0 n k :
  k <> 1 -> n < k -> binom n k = fact n / (fact k * fact (n - k)).
Proof.
intros Hk H; replace (n - k) with 0.
2: { apply eq_sym, Nat.sub_0_le. apply Nat.lt_le_incl. assumption. }
rewrite fact_0, Nat.mul_1_r. rewrite binom_eq_0. 2: assumption.
apply eq_sym, Nat.div_small. apply fact_lt. 2: assumption.
apply nat_eq_add_1_contra_wn1; assumption.
Qed.

(* "_wn0" means "when (something is) not 0", here "when binom n k <> 0". *)
Lemma binom_fact_div_wn0 n k :
  k <= n -> binom n k = fact n / (fact k * fact (n - k)).
Proof.
intros H. rewrite <- (binom_fact n k). 2: assumption.
apply eq_sym, Nat.div_mul. apply fact_mul_neq_0.
Qed.

(* Egalité avec l'expression utilisant factorielles et division euclidienne. /
 Equality with expression using factorials and Euclidean division. *)
Lemma binom_fact_div n k :
  (n <> 0 \/ k <> 1) -> binom n k = fact n / (fact k * fact (n - k)).
Proof.
intros H. destruct (le_lt_dec k n).
+ apply binom_fact_div_wn0. assumption.
+ apply binom_fact_div_w0. destruct H. 2,3: assumption.
  contradict H. subst. apply Nat.lt_1_r. assumption.
Qed.

Lemma binom_sym n k : k <= n ->  binom n k = binom n (n - k).
Proof.
intros H. rewrite !binom_fact_div.
+ rewrite Nat.mul_comm, nat_sub2, Nat.sub_diag, Nat.add_0_l.
  reflexivity. assumption. apply Nat.le_refl.
+ destruct (Nat.eq_dec n 0). 2: left; assumption.
  subst. right. rewrite Nat.sub_0_l. apply nat_neq_0_1.
+ destruct (Nat.eq_dec n 0). 2: left; assumption.
  subst. right. apply Nat.lt_neq, Nat.lt_succ_r. assumption.
Qed.

Lemma binom_sym_alt n k : binom (n + k) k = binom (n + k) n.
Proof. rewrite -> binom_sym, Nat.add_sub. reflexivity. apply Nat.le_add_l. Qed.

Lemma binom_Sn n : binom (S n) n = S n.
Proof.
rewrite -> binom_sym. 2: apply Nat.le_succ_diag_r.
rewrite <- Nat.add_1_l. rewrite Nat.add_sub. apply binom_1_r.
Qed.

(* Cancellation identity. *)
Lemma binom_cancellation n k i :
  binom n (k + i) * binom (k + i) i = binom n i * binom (n - i) k.
Proof.
destruct (nat_lt_le_dec n i) as [H | H].
+ rewrite (binom_eq_0 _ (_ + _)). 2: apply Nat.lt_lt_add_l; assumption.
  rewrite (binom_eq_0 n). reflexivity. assumption.
+ apply (Nat.mul_cancel_l _ _ (fact i * fact k * fact (n - i))).
  apply fact_mul2_neq_0.
  replace (_ * _ * _ * (_ * _)) with
      (fact (k + i) * fact (n - i) * binom n (k + i)).
  2: { rewrite <- (binom_fact (k + i) i). 2: apply Nat.le_add_l.
    rewrite Nat.add_sub, !Nat.mul_assoc, (Nat.mul_comm _ (binom _ i)).
    rewrite !Nat.mul_assoc. reflexivity. }
  replace (_ * _ * _ * _) with (fact n * fact k * binom (n - i) k).
  2: { rewrite <- (binom_fact n i). 2: assumption.
    rewrite <- (Nat.mul_assoc (fact i)), (Nat.mul_comm (fact k)).
    rewrite !Nat.mul_assoc. f_equal.
    rewrite (Nat.mul_comm _ (binom _ _)), !Nat.mul_assoc. reflexivity. }
  apply binom_committee_chair_iter. right. assumption.
Qed.

(* Variant of the cancellation identity. *)
Lemma binom_cancellation_alt n k i :
  i <= k \/ n < i -> binom n k * binom k i = binom n i * binom (n - i) (k - i).
Proof.
destruct (le_lt_dec i k) as [H | H1].
+ intros _. rewrite <- binom_cancellation. rewrite Nat.sub_add.
  reflexivity. assumption.
+ intros [H2 | H2].
  - contradict H2. apply Nat.nle_gt. assumption.
  - rewrite (binom_eq_0 k i). 2: assumption.
    rewrite (binom_eq_0 n i). 2: assumption.
    rewrite Nat.mul_0_l, Nat.mul_0_r. reflexivity.
Qed.

End Binom_Facts.


Section Binom_sum_Facts.

Lemma binom_rising_sum_l m n :
  sum_range 0 n (fun j => binom (m + j) m) = binom (S m + n) (S m).
Proof.
rewrite sum_range_0_l. induction n.
+ rewrite sum_n_1. rewrite !Nat.add_0_r. rewrite !binom_diag. reflexivity.
+ rewrite sum_n_ind_r. rewrite IHn. rewrite Nat.add_0_l. rewrite Nat.add_comm.
  rewrite Nat.add_succ_comm. apply binom_pascal.
Qed.

Lemma binom_rising_sum_r m n :
  sum_range 0 n (fun j => binom (m + j) j) = binom (S m + n) n.
Proof.
rewrite binom_sym_alt. rewrite <- binom_rising_sum_l.
apply sum_n_ext. intros k _. apply binom_sym_alt.
Qed.

(* Formule de la gouttière / Hockey stick identity.
 Indication: traiter à part le cas (n < k), puis prouver une version utilisant
 le nombre de termes sommés, par exemple par induction sur ce nombre. /
 Hint: treat separately the case (n < k), then prove a version using the
 number of terms in the sum, eg by induction on this number. *)
Lemma binom_hockey_stick n k :
  sum_range k n (fun i => binom i k) = binom (S n) (S k).
Proof.
destruct (nat_lt_le_dec n k) as [H | H].
+ rewrite binom_eq_0. 2: rewrite <- Nat.succ_lt_mono; assumption.
  apply sum_range_correct_r. assumption.
+ revert n k H.
  assert (H : forall p k, sum_n k p (fun i => binom i k) = binom (k + p) (S k)).
    intros p. induction p.
    - intros. rewrite sum_n_0. rewrite binom_eq_0. reflexivity.
      rewrite Nat.add_0_r. apply Nat.lt_succ_diag_r.
    - intros. rewrite sum_n_ind_r. rewrite IHp.
      rewrite Nat.add_succ_r. rewrite Nat.add_comm. apply binom_pascal.
  (* *)
  - intros n k Hk. rewrite sum_range_correct_l. 2: assumption.
    rewrite H. f_equal.
    rewrite Nat.add_1_r. rewrite <- Nat.sub_succ_l. 2: assumption.
    rewrite Nat.add_comm. rewrite Nat.sub_add. reflexivity.
    apply Nat.le_trans with n. assumption. apply Nat.le_succ_diag_r.
Qed.

(* Formule du binôme. / Binomial theorem. *)
Lemma binom_formula a b n :
  (a + b) ^ n = sum_range 0 n (fun k => binom n k * a ^ k * b ^ (n - k)).
Proof.
rewrite sum_range_0_l. induction n. reflexivity.
rewrite Nat.pow_succ_r'. rewrite IHn. clear IHn.
rewrite Nat.mul_add_distr_r. rewrite 2!sum_n_mul_add_distr_l.
(* Transform first sum (on the left). *)
rewrite sum_n_ext with (f2 := (fun k => binom n k * a ^ S k * b ^ (n - k))).
2: { intros k _. rewrite Nat.pow_succ_r'. rewrite 2!Nat.mul_assoc.
  rewrite (Nat.mul_comm a). rewrite Nat.mul_assoc. reflexivity. }
rewrite sum_n_shift_r with (t := 1). rewrite Nat.add_0_l.
rewrite sum_n_ind_r. replace (1 + n - 1) with n.
2: { rewrite Nat.add_comm. apply eq_sym, Nat.add_sub. }
rewrite binom_diag, Nat.mul_1_l. rewrite Nat.sub_diag, Nat.mul_1_r.
(* Transform second sum (on the left). *)
rewrite sum_n_ext with (f1 := fun k => b * _)
    (f2 := (fun k => binom n k * a ^ k * b ^ (S n - k))).
2: { intros k [h1 h2]. rewrite Nat.sub_succ_l.
  2: apply Nat.lt_succ_r; assumption.
  rewrite Nat.pow_succ_r'. rewrite 2!(Nat.mul_comm b).
  rewrite Nat.mul_assoc. reflexivity. }
rewrite sum_n_ind_l. rewrite binom_0_r, Nat.mul_1_l. rewrite Nat.sub_0_r.
(* Transform left term. *)
rewrite Nat.add_comm. rewrite Nat.add_assoc, <- (Nat.add_assoc (b ^ _)).
rewrite sum_n_add. rewrite <- Nat.add_assoc.
set (f := fun k => binom (S n) k * a ^ k * b ^ (S n - k)).
rewrite sum_n_ext with (f2 := f).
2: { intros k [h1 h2]. unfold f.
  assert (h2' : k <= n).
    apply Nat.lt_succ_r. rewrite <- Nat.add_1_l. assumption.
  replace (S (k - 1)) with k.
  2: { rewrite Nat.sub_1_r. rewrite Nat.succ_pred. reflexivity.
    contradict h1. subst. apply Nat.nle_gt. apply Nat.lt_0_1. }
  replace (n - (k - 1)) with (S n - k).
  2: { rewrite <- Nat.add_1_r. rewrite Nat.add_sub_swap. 2: assumption.
    rewrite nat_sub2. reflexivity. all: assumption. }
  rewrite <- 2!Nat.mul_add_distr_r. do 2 f_equal.
  rewrite Nat.add_comm, Nat.sub_1_r. apply eq_sym, binom_S_l.
  contradict h1. subst. apply Nat.nle_gt. apply Nat.lt_0_1. }
(* Transform third sum (on the right). *)
rewrite sum_n_ind_l. rewrite sum_n_ind_r. do 2 f_equal.
+ unfold f. rewrite binom_0_r, Nat.mul_1_l. reflexivity.
+ unfold f. rewrite binom_diag, Nat.sub_diag, Nat.pow_0_r.
  rewrite Nat.mul_1_l, Nat.mul_1_r. reflexivity.
Qed.

Lemma binom_formula_1_l x n :
  (1 + x) ^ n = sum_range 0 n (fun k => binom n k * x ^ k).
Proof.
rewrite binom_formula. rewrite !sum_range_0_l. apply eq_sym.
rewrite sum_n_reverse. apply sum_n_ext. intros k [_ Hk].
rewrite Nat.pow_1_l. rewrite Nat.mul_1_r. rewrite <- binom_sym. reflexivity.
apply Nat.lt_succ_r. assumption.
Qed.

(* Indication : remarquer que (2=1+1) et utiliser la formule du binôme. /
Hint: Note that (2=1+1) and use binomial theorem. *)
Lemma binom_sum n : sum_range 0 n (fun k => binom n k) = 2 ^ n.
Proof.
replace 2 with (1+1) by reflexivity.
rewrite binom_formula.
apply sum_range_ext. intros.
rewrite !Nat.pow_1_l. rewrite !Nat.mul_1_r. reflexivity.
Qed.

(* Formule de Vandermonde / Vandermonde's identity.
 Indication : proceder par induction sur n. /
 Hint: proceed by induction on n. *)
Lemma binom_vandermonde m n p :
  binom (m + n) p = sum_range 0 p (fun k => binom m k * binom n (p - k)).
Proof.
rewrite sum_range_0_l. revert m p. induction n; intros m p.
+ rewrite Nat.add_0_r. rewrite sum_n_ind_r.
  rewrite Nat.add_0_l, Nat.sub_diag, binom_diag, Nat.mul_1_r.
  apply nat_add_0_l_equiv. apply sum_n_eq_0. intros i [_ Hi].
  rewrite (binom_eq_0 0). rewrite Nat.mul_0_r; reflexivity.
  apply Nat.add_lt_mono_r with i. rewrite Nat.sub_add. assumption.
  apply Nat.lt_le_incl. assumption.
+ destruct p.
  - rewrite sum_n_1. rewrite !binom_0_r. reflexivity.
  - rewrite sum_n_ind_r. rewrite Nat.add_0_l, Nat.sub_diag. rewrite binom_0_r.
    rewrite sum_n_ext with (f2 :=
        fun k => binom m k * binom n (p - k) + binom m k * binom n (S p - k)).
    2: { intros k [_ Hk]. rewrite Nat.sub_succ_l.
      2: apply Nat.lt_succ_r; assumption.
      rewrite <- binom_pascal. rewrite Nat.mul_add_distr_l. reflexivity. }
    rewrite <- sum_n_add. rewrite <- Nat.add_assoc, <- (binom_0_r n).
    rewrite <- (Nat.sub_diag (S p)) at 3. rewrite <- sum_n_ind_r.
    rewrite Nat.add_succ_r. rewrite <- binom_pascal. rewrite !IHn. reflexivity.
Qed.

(* Indication : remarquer que (2*n=n+n) et utiliser la formule de Vandermonde. /
 Hint: notice that (2*n=n+n) and use Vandermonde's identity. *)
Lemma binom_sum_sqr n :
  sum_range 0 n (fun k => (binom n k)^2) = binom (2 * n) n.
Proof.
replace (2 * n) with (n + n) by apply Nat.double_twice.
rewrite binom_vandermonde.
apply sum_range_ext. intros k [_ Hk].
rewrite <- binom_sym. 2: assumption.
rewrite Nat.pow_2_r. reflexivity.
Qed.

End Binom_sum_Facts.


Section Fact_Compl.

Lemma fact_add_mod_0 : forall m n, fact (m + n) mod (fact m * fact n) = 0.
Proof.
intros m n. apply Nat.mod_divides. apply fact_mul_neq_0.
exists (binom (m + n) n). rewrite Nat.mul_comm, (Nat.mul_comm (fact _)).
rewrite <- (Nat.add_sub m n) at 3. rewrite binom_fact.
reflexivity. apply Nat.le_add_l.
Qed.

Lemma fact_sub_mod_0 :
  forall n k, k <= n -> (fact n) mod (fact k * fact (n - k)) = 0.
Proof.
intros n k H. replace n with (k + (n - k)) at 1. apply fact_add_mod_0.
rewrite Nat.add_comm. apply Nat.sub_add. assumption.
Qed.

End Fact_Compl.
