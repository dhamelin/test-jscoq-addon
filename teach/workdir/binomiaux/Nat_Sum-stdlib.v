
(* This part was removed for ThEdu'24, and Liber-Abaci.
But it should be pertinent for the standard library. *)

(* Somme de a inclus à b exclus.
 Sum from a included to b excluded.
 sum_range' a b f = f a + f (a + 1) + ... f (b - 1).
 b <= a -> sum_range' a b f = 0. *)
Definition sum_range' (a b : nat) (f : nat -> nat) : nat :=
  sum_n a (b - a) f.

Compute (sum_range' 2 4 (fun x => x)).
