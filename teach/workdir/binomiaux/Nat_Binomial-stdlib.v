
(* The remainder is removed for ThEdu'24, and Liber-Abaci.
But it should be pertinent for the standard library. *)

Section Pbinom_Def.

(** The definition pbinom provides a convenient way to structurally ensure that
 the binomial coefficient is at least 1.

 All lemmas with a name prefixed with "pbinomS_" only involve pbinom under the
 form "S (pbinom m n)". Other results have a name prefixed with "pbinom_". *)

Definition pbinom m n := pred (binom (m + n) m).

End Pbinom_Def.


Section PbinomS_Facts.

Lemma pbinomS_eq : forall m n, S (pbinom m n) = binom (m + n) m.
Proof.
intros; apply Nat.succ_pred, not_eq_sym, Nat.lt_neq, binom_gt_0, Nat.le_add_r.
Qed.

Lemma pbinomS_pascal :
  forall m n, S (pbinom m (S n)) + S (pbinom (S m) n) = S (pbinom (S m) (S n)).
Proof.
intros; rewrite !pbinomS_eq, <- Nat.add_succ_comm, Nat.add_succ_r;
    apply binom_pascal.
Qed.

Lemma pbinomS_sym : forall m n, S (pbinom m n) = S (pbinom n m).
Proof. intros; rewrite !pbinomS_eq, Nat.add_comm; apply binom_sym_alt. Qed.

Lemma pbinomS_0_l : forall n, S (pbinom 0 n) = 1.
Proof. intros; rewrite pbinomS_eq, Nat.add_0_l; apply binom_0_r. Qed.

Lemma pbinomS_0_r : forall m, S (pbinom m 0) = 1.
Proof. intros; rewrite pbinomS_eq, Nat.add_0_r; apply binom_diag. Qed.

Lemma pbinomS_1_l : forall n, S (pbinom 1 n) = S n.
Proof. intros; rewrite pbinomS_eq, Nat.add_1_l; apply binom_1_r. Qed.

Lemma pbinomS_1_r : forall m, S (pbinom m 1) = S m.
Proof. intros; rewrite pbinomS_eq, Nat.add_1_r; apply binom_Sn. Qed.

Lemma pbinomS_ge_0 : forall m n, 0 <= S (pbinom m n).
Proof. intros; rewrite pbinomS_eq; apply binom_ge_0. Qed.

Lemma pbinomS_gt_0 : forall m n, 0 < S (pbinom m n).
Proof. intros; rewrite pbinomS_eq; apply binom_gt_0, Nat.le_add_r. Qed.

Lemma pbinomS_le_S : forall m n, S (pbinom m n) <= S (pbinom m (S n)).
Proof. intros; rewrite !pbinomS_eq, Nat.add_succ_r; apply binom_le_S. Qed.

Lemma pbinomS_le_pred : forall m n, S (pbinom m (pred n)) <= S (pbinom m n).
Proof.
intros m [| n]; [rewrite Nat.pred_0; apply Nat.le_refl |].
rewrite !pbinomS_eq, Nat.add_pred_r;
    [apply binom_le_pred | apply Nat.neq_succ_0].
Qed.

Lemma pbinomS_le :
  forall m {n1 n2}, n1 <= n2 -> S (pbinom m n1) <= S (pbinom m n2).
Proof.
intros; rewrite !pbinomS_eq; apply binom_le, Nat.add_le_mono_l; assumption.
Qed.

Lemma pbinomS_lt_S :
  forall {m} n, m <> 0 -> S (pbinom m n) < S (pbinom m (S n)).
Proof.
intros; rewrite !pbinomS_eq, Nat.add_succ_r; apply binom_lt_S; split.
+ apply Nat.neq_0_lt_0; assumption.
+ rewrite <- Nat.add_succ_r; apply Nat.le_add_r.
Qed.

Lemma pbinomS_lt_pred :
  forall {m n}, m <> 0 -> n <> 0 -> S (pbinom m (pred n)) < S (pbinom m n).
Proof.
intros; rewrite !pbinomS_eq, Nat.add_pred_r; [| assumption].
apply binom_lt_pred; split;
    [apply Nat.neq_0_lt_0; assumption | apply Nat.le_add_r].
Qed.

Lemma pbinomS_lt :
  forall {m n1 n2}, m <> 0 -> n1 < n2 -> S (pbinom m n1) < S (pbinom m n2).
Proof.
intros; rewrite !pbinomS_eq; apply binom_lt.
+ split. apply Nat.neq_0_lt_0; assumption. rewrite <- Nat.add_succ_r; apply Nat.le_add_r.
+ apply Nat.add_lt_mono_l; assumption.
Qed.

Lemma pbinomS_gt_1 : forall {m n}, m <> 0 -> n <> 0 -> 1 < S (pbinom m n).
Proof.
intros; rewrite pbinomS_eq; apply binom_gt_1; split;
    [| apply Nat.lt_add_pos_r]; apply Nat.neq_0_lt_0; assumption.
Qed.

Lemma pbinomS_gt_1_contra : forall {m n}, S (pbinom m n) <= 1 -> m = 0 \/ n = 0.
Proof.
intros m n; rewrite pbinomS_eq; intros H; apply binom_gt_1_contra in H.
destruct H as [H | H]; [left; apply Nat.le_0_r; assumption |].
right; destruct (Nat.eq_dec n 0); [assumption |]; contradict H.
apply Nat.nle_gt, Nat.lt_add_pos_r, Nat.neq_0_lt_0; assumption.
Qed.

Lemma pbinomS_eq_1_equiv : forall {m n}, S (pbinom m n) = 1 <-> m = 0 \/ n = 0.
Proof.
intros; rewrite pbinomS_eq, binom_eq_1_equiv, nat_add_0_r_equiv; apply iff_refl.
Qed.

Lemma pbinomS_mul_diag :
  forall m n, n <> 0 ->
    (m + n) * S (pbinom m (pred n)) = S m * S (pbinom (S m) (pred n)).
Proof.
intros; rewrite !pbinomS_eq, Nat.add_pred_r, nat_add_S_pred, pion_formula;
    [reflexivity | assumption..].
Qed.

Lemma pbinomS_fact :
  forall {m n}, S (pbinom m n) * (fact m * fact n) = fact (m + n).
Proof.
intros; rewrite pbinomS_eq; rewrite <- (Nat.add_sub n m) at 2;
    rewrite (Nat.add_comm n); apply binom_fact, Nat.le_add_r.
Qed.

Lemma pbinomS_fact_div :
  forall m n, S (pbinom m n) = fact (m + n) / (fact m * fact n).
Proof.
intros; rewrite pbinomS_eq, binom_fact_div.
+ rewrite (Nat.add_comm m n) at 2; rewrite Nat.add_sub; reflexivity.
+ destruct (Nat.eq_dec m 1); [| right; assumption].
  left; subst; rewrite Nat.add_1_l; apply Nat.neq_succ_0.
Qed.

End PbinomS_Facts.


Section Pbinom_Facts.

Lemma pbinom_sym : forall m n, pbinom m n = pbinom n m.
Proof. intros; apply Nat.succ_inj, pbinomS_sym. Qed.

Lemma pbinom_0_l : forall n, pbinom 0 n = 0.
Proof. intros; apply Nat.succ_inj, pbinomS_0_l. Qed.

Lemma pbinom_0_r : forall m, pbinom m 0 = 0.
Proof. intros; apply Nat.succ_inj, pbinomS_0_r. Qed.

Lemma pbinom_1_l : forall n, pbinom 1 n = n.
Proof. intros; apply Nat.succ_inj, pbinomS_1_l. Qed.

Lemma pbinom_1_r : forall m, pbinom m 1 = m.
Proof. intros; apply Nat.succ_inj, pbinomS_1_r. Qed.

Lemma pbinom_le : forall m n1 n2, n1 <= n2 -> pbinom m n1 <= pbinom m n2.
Proof. intros; apply Nat.succ_le_mono, pbinomS_le; easy. Qed.

Lemma pbinom_ge_0 : forall m n, 0 <= pbinom m n.
Proof. intros; apply Nat.lt_succ_r, pbinomS_gt_0. Qed.

End Pbinom_Facts.
