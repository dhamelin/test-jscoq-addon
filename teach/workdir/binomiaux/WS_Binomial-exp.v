(**
This file is part of the Liber-Abaci library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** Worksheet about iterated sums, factorials and binomials.

 These are alternate proofs of already proves general results,
 or specific results. *)

From LiberAbaci Require Import nat_helper.


(*
Affirmation (dire si V/F et justifier) : (n+3 n) est un multiple de 5
Résoudre (n 5) = 17 (n 4) avec n>=5
Résoudre (2n 1) + (2n 2) + (2n 3) = 387n avec n >= 2
https://www.bibmath.net/ressources/index.php?action=affiche&quoi=bde/proba/denombrement-binomiaux&type=fexo
*)
