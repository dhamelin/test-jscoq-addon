(**
This file is part of the Liber-Abaci library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** Additional results on N, the set of natural numbers. *)

From Coq Require Import PeanoNat Compare_dec.


Local Open Scope nat_scope.


Section Nat_eq_Compl.

(** Compléments sur l'égalité. / Complements on equality. *)

Lemma nat_neq_0_1 : 0 <> 1.
Proof. apply Nat.lt_neq, Nat.lt_0_1. Qed.

Lemma nat_eq_1_equiv {n} : n = 1 <-> n <> 0 /\ n <= 1.
Proof.
split.
+ intros; subst; split. apply not_eq_sym, nat_neq_0_1. apply Nat.le_refl.
+ intros [H1 H2]; apply Nat.le_1_r in H2; destruct H2; subst. contradict H1.
  all: reflexivity.
Qed.

End Nat_eq_Compl.


Section Nat_ineq_Compl.

(** Compléments sur les inégalités. / Complements on inequalities. *)

Lemma nat_sub_0_eq m n : m - n = 0 /\ n - m = 0 <-> m = n.
Proof.
rewrite !Nat.sub_0_le; split.
+ intros [H1 H2]; apply Nat.le_antisymm; assumption.
+ intros; subst; split; apply Nat.le_refl.
Qed.

(* "_wnl" means "when (something is) not last", here "when m <> S n". *)
Lemma nat_le_S_wnl {m n} : m <= S n -> m <> S n -> m <= n.
Proof.
intros H1 H2; apply Nat.nlt_ge; contradict H2.
apply Nat.le_antisymm. 2: apply Nat.le_succ_l. all: assumption.
Qed.

(* "_wnl" means "when (something is) not last", here "when m <> n". *)
Lemma nat_lt_S_wnl {m n} : m < S n -> m <> n -> m < n.
Proof.
intros H1 H2; apply Nat.nle_gt; contradict H2.
apply Nat.le_antisymm. apply Nat.lt_succ_r. all: assumption.
Qed.

(* "_wnf" means "when (something is) not first", here "when n <> S m". *)
Lemma nat_lt_S_wnf {m n} : m < n -> n <> S m -> S m < n.
Proof.
intros H1 H2; apply Nat.nle_gt; contradict H2.
apply Nat.le_antisymm. 2: apply Nat.le_succ_l. all: assumption.
Qed.

Lemma nat_S2_gt_1 n : 1 < S (S n).
Proof.
induction n. apply Nat.lt_1_2.
apply Nat.lt_trans with (S (S n)). assumption.
apply Nat.lt_succ_diag_r.
Qed.

Lemma nat_lt_le_dec m n : {m < n} + {n <= m}.
Proof. destruct (le_lt_dec n m). right; assumption. left; assumption. Qed.

End Nat_ineq_Compl.


Section Nat_add_Compl.

(** Compléments sur l'addition. / Complements on addition. *)

Lemma nat_add_0_r_equiv m n : m = m + n <-> n = 0.
Proof.
split; intros H.
+ rewrite <- (Nat.add_0_l n), <- (Nat.sub_diag m) at 1.
  rewrite <- Nat.add_sub_swap, <- H, Nat.sub_diag.
  reflexivity. apply Nat.le_refl.
+ subst; apply eq_sym, Nat.add_0_r.
Qed.

Lemma nat_add_0_l_equiv m n : n = m + n <-> m = 0.
Proof. rewrite Nat.add_comm. apply nat_add_0_r_equiv. Qed.

Lemma nat_add_S_pred m {n} : n <> 0 -> S m + pred n = m + n.
Proof.
intros; rewrite Nat.add_pred_r, Nat.add_succ_l. 2: assumption.
apply Nat.pred_succ.
Qed.

Lemma nat_add_pred_S {m} n : m <> 0 -> pred m + S n = m + n.
Proof.
intros; rewrite Nat.add_pred_l, Nat.add_succ_r. 2: assumption.
apply Nat.pred_succ.
Qed.

(* Partial contrapositive of Nat.eq_add_1.
 "_wn0" means "when (something is) not 0", here "when m <> 0 /\ n <> 0". *)
Lemma nat_eq_add_1_contra_wn0 {m n} : m <> 0 -> n <> 0 -> m < n -> m + n <> 1.
Proof.
intros H1 H2 H; contradict H1; apply Nat.eq_add_1 in H1.
destruct H1 as [[Hm Hn] | [Hm Hn]]; subst. contradict H2. all: reflexivity.
Qed.

(* Partial contrapositive of Nat.eq_add_1.
 "_wn1" means "when (something is) not 1", here "when n <> 1". *)
Lemma nat_eq_add_1_contra_wn1 {m n} : n <> 1 -> m < n -> m + n <> 1.
Proof.
intros Hn H; contradict Hn; apply Nat.eq_add_1 in Hn.
destruct Hn as [[Hm Hn] | [Hm Hn]]; subst. 2: reflexivity.
contradict H. apply Nat.nlt_ge, Nat.le_0_1.
Qed.

Lemma nat_lt_add_l_S m n p : m <= p -> m < n + S p.
Proof. rewrite <- Nat.lt_succ_r; apply Nat.lt_lt_add_l. Qed.

(* "_wn0" means "when (something is) not 0", here "when n <> 0". *)
Lemma nat_lt_add_l_wn0 m n p : n <> 0 -> m <= p -> m < n + p.
Proof.
intros Hn H. rewrite <- (Nat.succ_pred n). 2: assumption.
rewrite Nat.add_succ_comm. apply nat_lt_add_l_S. assumption.
Qed.

Lemma nat_add_lt_l_S m n : m + n < m + S n.
Proof. apply Nat.add_lt_mono_l, Nat.lt_succ_diag_r. Qed.

Lemma nat_lt_add_S m n p : p < m + n -> p < m + S n.
Proof.
intros. apply Nat.lt_trans with (m + n). assumption. apply nat_add_lt_l_S.
Qed.

Lemma nat_le_add_r_lt_S m n : m <= m + n < m + S n.
Proof. split. apply Nat.le_add_r. apply nat_add_lt_l_S. Qed.

Lemma nat_le_lt_add_S m n p : m <= p < m + n -> m <= p < m + S n.
Proof. intros [H1 H2]. split. assumption. apply nat_lt_add_S. assumption. Qed.

Lemma nat_lt_add_succ_r m n p : m < n + S p <-> m <= n + p.
Proof. rewrite <- Nat.lt_succ_r. rewrite Nat.add_succ_r. apply iff_refl. Qed.

Lemma nat_le_lt_add_succ_r m n p : m <= p < m + S n <-> m <= p <= m + n.
Proof. rewrite nat_lt_add_succ_r. apply iff_refl. Qed.

Lemma nat_le_lt_add_sub m n p : m <= p < m + n -> 0 <= p - m < n.
Proof.
intros [H1 H2]; split.
+ apply Nat.le_0_l.
+ apply (Nat.add_lt_mono_l _ _ m). rewrite Nat.add_comm.
  rewrite Nat.sub_add. all: assumption.
Qed.

End Nat_add_Compl.


Section Nat_sub_Compl.

(** Compléments sur la soustraction. / Complements on subtraction. *)

Lemma nat_pred_sub m n : pred (m - n) = pred m - n.
Proof.
revert m; induction n.
+ intros; rewrite !Nat.sub_0_r. reflexivity.
+ intros; rewrite !Nat.sub_succ_r. rewrite IHn; reflexivity.
Qed.

Lemma nat_sub2 {m n p} : p <= n -> n <= m -> m - (n - p) = m - n + p.
Proof.
intros H1 H2; apply Nat.add_sub_eq_r.
rewrite Nat.add_sub_assoc. 2: assumption.
rewrite <- Nat.add_assoc, (Nat.add_comm p), Nat.add_assoc.
rewrite <- Nat.add_sub_assoc. 2: apply Nat.le_refl.
rewrite Nat.sub_diag, Nat.add_0_r.
apply Nat.sub_add; assumption.
Qed.

Lemma nat_sub_0_le {m n} : m <= n -> m - n = 0.
Proof. intros; apply Nat.sub_0_le; assumption. Qed.

Lemma nat_sub_0_lt {m n} : m < n -> m - n = 0.
Proof. intros; apply nat_sub_0_le, Nat.lt_le_incl; assumption. Qed.

Lemma nat_sub_n0_lt m n : m - n <> 0 <-> n < m.
Proof.
split. 2: apply Nat.sub_gt.
intros H; apply Nat.nle_gt. contradict H. apply Nat.sub_0_le; assumption.
Qed.

Lemma nat_sub_lt_mono_rev {m n} p q : p <= q -> m - p < n - q -> m < n.
Proof.
intros H1 H2. destruct (le_lt_dec q n) as [H3 | H3].
2: { contradict H2. apply Nat.nlt_ge. replace (n - q) with 0.
    apply Nat.le_0_l. apply eq_sym, Nat.sub_0_le, Nat.lt_le_incl. assumption. }
destruct (le_lt_dec p m) as [H4 | H4].
2: { apply Nat.lt_le_trans with p. assumption.
    apply Nat.le_trans with q; assumption. }
specialize (Nat.add_le_lt_mono _ _ _ _ H1 H2).
rewrite Nat.add_comm. rewrite (Nat.add_comm q).
rewrite !Nat.sub_add. 2,3: assumption. tauto.
Qed.

End Nat_sub_Compl.


Section Nat_mul_Compl.

(** Compléments sur la multiplication. / Complements on multiplication. *)

Lemma nat_le_mul_S_l m n : m <= S n * m.
Proof. apply Nat.le_add_r. Qed.

Lemma nat_le_mul_S_r m n : m <= m * S n.
Proof. rewrite Nat.mul_comm; apply nat_le_mul_S_l. Qed.

Lemma nat_lt_mul_S_l m n : 0 < m -> 0 < n -> m < S n * m.
Proof. intros; apply Nat.lt_add_pos_r, Nat.mul_pos_pos; assumption. Qed.

Lemma nat_lt_mul_S_r m n : 0 < m -> 0 < n -> m < m * S n.
Proof. rewrite Nat.mul_comm; apply nat_lt_mul_S_l. Qed.

Lemma nat_lt_1_mul_pos_sym m n : 1 < m -> 0 < n -> 1 < n * m.
Proof. rewrite Nat.mul_comm. apply Nat.lt_1_mul_pos. Qed.

End Nat_mul_Compl.


Section Nat_div_Compl.

(** Complément sur la division. / Complements on division. *)

Lemma nat_div_mul_sym a b : b <> 0 -> b * a / b = a.
Proof. rewrite Nat.mul_comm. apply Nat.div_mul. Qed.

End Nat_div_Compl.


Section Nat2_Compl.

(** Compléments sur N^2. / Complements on N^2. *)

(* Principe d'induction double. / Double induction principle. *)
Lemma nat2_ind (P : nat -> nat -> Prop) :
  P 0 0 ->
  (forall m n, P m n -> P (S m) n) ->
  (forall m n, P m n -> P m (S n)) ->
  forall m n, P m n.
Proof.
intros H00 HSl HSr; induction m; intros n.
+ induction n. assumption. apply HSr; assumption.
+ apply HSl, IHm.
Qed.

(* Principe d'induction double pour une propriété symétrique. /
 Double induction principle for a symmetric property. *)
Lemma nat2_ind_sym (P : nat -> nat -> Prop) :
  (forall m n, P m n -> P n m) ->
  P 0 0 ->
  (forall m n, P m n -> P (S m) n) ->
  forall m n, P m n.
Proof.
intros HP P00 HS; apply nat2_ind.
+ assumption.
+ assumption.
+ intros; apply HP, HS, HP; assumption.
Qed.

(* Décidabilité de deux égalités. / Decidability of two equalities. *)
Lemma nat2_eq_dec (m1 n1 m2 n2 : nat) :
  { m1 = n1 /\ m2 = n2 } + { m1 <> n1 \/ m2 <> n2 }.
Proof.
destruct (Nat.eq_dec m1 n1) as [H1 | H1].
destruct (Nat.eq_dec m2 n2) as [H2 | H2].
+ left; split; assumption.
+ right; right; assumption.
+ right; left; assumption.
Qed.

(* Décidabilité d'une égalité et d'une inégalité (large). /
 Decidability of an equality and a (large) inequality. *)
Lemma nat2_eq_le_dec (m1 n1 m2 n2 : nat) :
  { m1 = n1 /\ m2 <= n2 } + { m1 <> n1 \/ n2 < m2 }.
Proof.
destruct (Nat.eq_dec m1 n1) as [H1 | H1].
destruct (le_lt_dec m2 n2) as [H2 | H2].
+ left; split; assumption.
+ right; right; assumption.
+ right; left; assumption.
Qed.

(* Décidabilité d'une égalité et d'une inégalité (stricte). /
 Decidability of an equality and a (strict) inequality. *)
Lemma nat2_eq_lt_dec (m1 n1 m2 n2 : nat) :
  { m1 = n1 /\ m2 < n2 } + { m1 <> n1 \/ n2 <= m2 }.
Proof.
destruct (Nat.eq_dec m1 n1) as [H1 | H1].
destruct (nat_lt_le_dec m2 n2) as [H2 | H2].
+ left; split; assumption.
+ right; right; assumption.
+ right; left; assumption.
Qed.

End Nat2_Compl.
