(**
This file is part of the Liber-Abaci library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** Worksheet on binomial coefficients.

 These are:
 - general statements from library modules Nat_Sum, Nat_Factorial and
   Nat_Binomial that are equipped with guidance comments for students, and
   solution(s) for teachers;
 - alternate proofs general results from the library modules;
 - proofs that hypotheses of general results are optimal;
 - or specific statements or questions.

 Note that provided script XXX will replace lines from marker
   (* Begin solution for the teacher. *)
 to next marker (included)
   (* End solution for the teacher. *)
 by "Admitted.", and will remove lines from marker
   (* Begin other solution for the teacher. *)
 to next marker (included)
   (* End other solution for the teacher. *)

 That way, both teacher and student versions of the file are compiling, and
 teachers can verify the validity of the provided solution(s). *)

From LiberAbaci Require Import WS_Helper.


Section Statements_From_Lib.

(* Faire la preuve du lemme suivant et remplacer "Admitted" par "Qed". /
Fill the proof of next lemma and replace "Admitted" by "Qed". *)
Lemma binom_sum_sqr n :
  sum_range 0 n (fun k => (binom n k) ^ 2) = binom (2 * n) n.
Proof.
(* Transformer (2*n) en une somme. / Transform (2*n) into a sum. *)
(* Utiliser l'identité de Vandermonde. / Use Vandermonde's identity. *)
(* Réduire l'égalite de somme à l'égalité des fonctions. /
 Reduce equality on sums to equality on functions. *)
(* Utiliser la symétrie des coefficients binomiaux. /
 Use symmetry of binomial coefficients. *)
(* Ne pas oublier les conditions d'application des lemmes utilisés. /
 Do not forget application conditions of used lemmas. *)

(* Begin solution for the teacher. *)
(* Without automation. *)
replace (2 * n) with (n + n) by apply double_twice.
rewrite binom_vandermonde.
apply sum_range_ext. intros k [_ Hk].
rewrite <- binom_sym. 2: assumption.
rewrite pow_2_r. reflexivity.
Qed.
(* End solution for the teacher. *)

(* Begin other solution for the teacher. *)
(* With automation. *)
Lemma binom_sum_sqr_with_auto n :
  sum_range 0 n (fun k => (binom n k) ^ 2) = binom (2 * n) n.
Proof.
replace (2 * n) with (n + n) by lia.
rewrite binom_vandermonde.
apply sum_range_ext. intros k Hk.
rewrite <- binom_sym by lia.
now rewrite pow_2_r.
Qed.
(* End other solution for the teacher. *)

(* Faire la preuve du lemme suivant et remplacer "Admitted" par "Qed". /
Fill the proof of next lemma and replace "Admitted" by "Qed". *)
Lemma binom_sum n : sum_range 0 n (fun k => binom n k) = 2 ^ n.
Proof.
(* Transformer 2 en une somme. / Transform 2 into a sum. *)
(* Utiliser la formule du binome. / Use binomial theorem. *)
(* Reduire l'egalite de somme a l'egalite des fonctions. /
 Reduce equality on sums to equality on functions. *)

(* Begin solution for the teacher. *)
(* Without automation. *)
replace 2 with (1+1) by reflexivity.
rewrite binom_formula.
apply sum_range_ext. intros k Hk.
rewrite !pow_1_l.
rewrite !mul_1_r. reflexivity.
Qed.
(* End solution for the teacher. *)

(* Begin other solution for the teacher. *)
(* With automation. *)
Lemma binom_sum_with_auto n : sum_range 0 n (fun k => binom n k) = 2 ^ n.
Proof.
replace 2 with (1+1) by easy.
rewrite binom_formula.
apply sum_range_ext. intros k Hk.
rewrite !pow_1_l; lia.
Qed.
(* End other solution for the teacher. *)

End Statements_From_Lib.


Section Alternate_Proofs.

(* Preuve alternative de la formule du pion. /
 Alternate proof of the committee-chair identity. *)
Lemma binom_committee_chair_alt n k :
  S k * binom n (S k) = n * binom (n - 1) k.
Proof.
  destruct n. now simpl.
  destruct (le_lt_dec (S k) (S n)) as [h1|]. 2: rewrite 2!binom_eq_0; lia.
  (* Hypothèse pour aider lia / hypothesis to help lia. *)
  assert (hfnk: fact (n - k) <> 0). apply fact_neq_0.
  assert (hfk: fact k <> 0). apply fact_neq_0.
  assert (hfSk: fact (S k) <> 0). apply fact_neq_0.
  assert (hfSnSk: fact (S n - S k) <> 0). apply fact_neq_0.

  symmetry. (* On fait la preuve à l'envers par rapport math-os *)
  rewrite binom_fact_div_wn0. 2: lia.
  replace (S n - 1) with n by lia.

  rewrite <- divide_div_mul_exact; try lia.
  * rewrite mul_comm, <- fact_S.
    rewrite <- div_mul_cancel_l with (c := S k); try lia.
    rewrite mul_assoc.
    rewrite (mul_comm _ (fact k)), <- fact_S.
    replace (n - k) with (S n - S k) by lia.
    rewrite divide_div_mul_exact; try lia.
    + now rewrite <- binom_fact_div_wn0.
    + apply mod_divide. lia.
      apply fact_sub_mod_0; lia.
  * apply mod_divide. lia.
    apply fact_sub_mod_0; lia.
Qed.

End Alternate_Proofs.


Section Optimality_Of_Hypotheses.

(* Prouver que les hypothèses des lemmes suivants sont optimales : /
 Prove that hypotheses of the following lemmas are optimal:
   binom_S_l, binom_S_r, binom_eq_0, binom_gt_0, binom_neq_0, binom_le_alt,
   binom_lt_S, binom_lt, binom_lt_alt, binom_gt_1, binom_committee_chair_iter,
   binom_fact, binom_fact_div, binom_sym, binom_cancellation_alt.

Indication : ces lemmes sont de la forme (P -> Q). Il faut donc prouver
 l'équivalence (P <-> Q), ou juste l'implication dans l'autre sens (Q -> P),
 ou plutôt la contraposée de cette dernière (~ P -> ~ Q). /
Hint: these lemmas are of the form (P -> Q). It is thus a matter of proving
 either the equivalence (P <-> Q), or just the reverse implication (Q -> P),
 or rather the contrapositive of the latter (~ P -> ~ Q). *)

(* "_rev_contra" means the contrapositive of the reverse implication. *)

Lemma binom_S_l_rev_contra n k :
  k = 0 -> binom (S n) k <> binom n (pred k) + binom n k.
Proof.
intros; subst. simpl.
rewrite !binom_0_r.
apply lt_neq, lt_1_2.
Qed.

(* Ici, P est de la forme (~ P1 \/ ~ P2), sa négation est donc (P1 /\ P2). /
 Here, P is of the form (~ P1 \/ ~ P2), thus its negation is (P1 /\ P2). *)
Lemma binom_S_r_rev_contra n k :
  n = 0 /\ k = 0 -> binom n (S k) <> binom (pred n) k + binom (pred n) (S k).
Proof.
intros [Hn Hk]; subst. simpl.
apply nat_neq_0_1.
Qed.

Lemma binom_eq_0_rev_contra n k : k <= n -> binom n k <> 0.
Proof. apply binom_neq_0. Qed.

Lemma binom_gt_0_rev_contra n k : n < k-> binom n k <= 0.
Proof. rewrite le_0_r. apply binom_eq_0. Qed.

Lemma binom_neq_0_rev_contra n k : n < k -> binom n k = 0.
Proof. apply binom_eq_0. Qed.

(* Ici, P est de la forme (P1 \/ P2 \/ P3), sa négation est donc
 (~ P1 /\ ~ P2 /\ ~ P3). /
 Here, P is of the form (P1 \/ P2 \/ P3), thus its negation is
 (~ P1 /\ ~ P2 /\ ~ P3). *)
Lemma binom_le_alt_rev_contra n1 n2 k :
  (k <> 0 /\ k <= n1 /\ n2 < n1) -> binom n2 k < binom n1 k.
Proof.
intros [Hk1 [Hk2 Hn]]. apply binom_lt. 2: assumption. split. 2: assumption.
apply neq_0_lt_0. assumption.
Qed.

(* Ici, P est de la forme (P1 /\ P2), sa négation est donc (~ P1 \/ ~ P2). /
 Here, P is of the form (P1 /\ P2), thus its negation is (~ P1 \/ ~ P2). *)
Lemma binom_lt_S_rev_contra n k :
  (k <= 0 \/ S n < k) -> binom (S n) k <= binom n k.
Proof.
intros [Hk | H].
+ rewrite le_0_r in Hk. subst. simpl. rewrite binom_0_r. apply le_refl.
+ rewrite binom_eq_0. apply le_0_l. assumption.
Qed.

(* Ici, P est de la forme (P1 /\ P2 /\ P3), sa négation est donc
 (~ P1 \/ ~ P2 \/ ~ P3). /
 Here, P is of the form (P1 /\ P2 /\ P3), thus its negation is
 (~ P1 \/ ~ P2 \/ ~ P3). *)
Lemma binom_lt_rev_contra n1 n2 k :
  (k <= 0 \/ n2 < k \/ n2 <= n1) -> binom n2 k <= binom n1 k.
Proof. rewrite le_0_r. apply binom_le_alt. Qed.

(* Ici, P est de la forme (P1 /\ P2), sa négation est donc (~ P1 \/ ~ P2). /
 Here, P is of the form (P1 /\ P2), thus its negation is (~ P1 \/ ~ P2). *)
Lemma binom_gt_1_rev_contra n k : k <= 0 \/ n <= k -> binom n k <= 1.
Proof.
rewrite le_0_r. intros [H | H].
+ subst. rewrite binom_0_r. apply le_refl.
+ destruct (le_lt_eq_dec _ _ H) as [H' | H'].
  - rewrite binom_eq_0. apply le_0_l. assumption.
  - subst. rewrite binom_diag. apply le_refl.
Qed.

(* Ici, P est de la forme (~ P1 \/ P2), sa négation est donc (P1 /\ ~ P2). /
 Here, P is of the form (~ P1 \/ P2), thus its negation is (P1 /\ ~ P2). *)
Lemma binom_committee_chair_iter_rev_contra n k i :
  (k = 0 /\ n < i) ->
  fact (k + i) * fact (n - i) * binom n (k + i) <>
    fact n * fact k * binom (n - i) k.
Proof.
intros [Hk H]; subst. simpl.
rewrite binom_eq_0. 2: assumption.
rewrite mul_0_r.
rewrite nat_sub_0_lt. 2: assumption. simpl.
rewrite !mul_1_r.
apply not_eq_sym, fact_neq_0.
Qed.

Lemma binom_fact_rev_contra n k :
  n < k -> binom n k * (fact k * fact (n - k)) <> fact n.
Proof.
intros H.
rewrite binom_eq_0. 2: assumption. simpl.
apply not_eq_sym, fact_neq_0.
Qed.

(* Ici, P est de la forme (~ P1 \/ ~ P2), sa négation est donc (P1 /\ P2). /
 Here, P is of the form (~ P1 \/ ~ P2), thus its negation is (P1 /\ P2). *)
Lemma binom_fact_div_rev_contra n k :
  (n = 0 /\ k = 1) -> binom n k <> fact n / (fact k * fact (n - k)).
Proof. intros [Hn Hk]; subst. simpl. apply nat_neq_0_1. Qed.

Lemma binom_sym_rev_contra n k : n < k ->  binom n k <> binom n (n - k).
Proof.
intros H.
rewrite binom_eq_0. 2: assumption.
rewrite nat_sub_0_lt. 2: assumption.
rewrite binom_0_r.
apply nat_neq_0_1.
Qed.

(* Ici, P est de la forme (P1 \/ P2), sa négation est donc (~ P1 /\ ~ P2). /
 Here, P is of the form (P1 \/ P2), thus its negation is (~ P1 /\ ~ P2). *)
Lemma binom_cancellation_alt_rev_contra n k i :
  k < i /\ i <= n ->
  binom n k * binom k i <> binom n i * binom (n - i) (k - i).
Proof.
intros [H1 H2].
rewrite (binom_eq_0 k i H1). rewrite mul_0_r.
apply not_eq_sym, neq_mul_0. split; apply binom_neq_0.
+ assumption.
+ apply sub_le_mono_r.
  apply lt_le_incl, lt_le_trans with i.
  all: assumption.
Qed.

End Optimality_Of_Hypotheses.


Section Specific_Statements.
End Specific_Statements.
