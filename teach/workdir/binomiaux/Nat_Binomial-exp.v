(**
This file is part of the Liber-Abaci library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** Support for binomial coefficients in N, the set of natural numbers. *)

From Coq Require Import PeanoNat Compare_dec.

From LiberAbaci Require Import Nat_Compl Nat_Sum Nat_Factorial.


Local Open Scope nat_scope.


Section Binom_sum_Facts.

(*(* Indication : utiliser poly_mul. *)
(* Hint: use poly_mul. *)
Lemma binom_vandermonde a b n :
  n <= a + b ->
  binom (a + b) n = sum_n 0 (S n) (fun k => binom a k * binom b (n - k)).
Proof.
revert n. apply poly_inj. intros x. rewrite <- binom_formula_1_l.
rewrite Nat.pow_add_r. rewrite !binom_formula_1_l. rewrite poly_mul.
reflexivity. all: apply binom_eq_0.
Qed.*)

End Binom_sum_Facts.
