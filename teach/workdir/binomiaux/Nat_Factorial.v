(**
This file is part of the Liber-Abaci library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** Support for factorials in N, the set of natural numbers. *)

From Coq Require Import PeanoNat Compare_dec.

From LiberAbaci Require Import Nat_Compl.


Local Open Scope nat_scope.


Section Fact_Def.

(* Factorielle de n. / Factorial of n.
 fact n = 1 * 2 * ... * (n - 1) * n. *)
Fixpoint fact (n : nat) : nat :=
  match n with
  | O => 1
  | S n' => fact n' * S n'
  end.

End Fact_Def.


Section Fact_Facts.

Lemma fact_0 : fact 0 = 1.
Proof. reflexivity. Qed.

Lemma fact_1 : fact 1 = 1.
Proof. reflexivity. Qed.

Lemma fact_S n : fact (S n) = fact n * S n.
Proof. reflexivity. Qed.

Lemma fact_pred n : fact n = fact (S n) / S n.
Proof. rewrite fact_S, Nat.div_mul. reflexivity. discriminate. Qed.

Lemma fact_gt_0 n : 0 < fact n.
Proof.
induction n. apply Nat.lt_0_1.
rewrite fact_S; apply Nat.mul_pos_pos. apply IHn. apply Nat.lt_0_succ.
Qed.

Lemma fact_neq_0 n : fact n <> 0.
Proof. apply Nat.neq_0_lt_0, fact_gt_0. Qed.

Lemma fact_mul_neq_0 m n : fact m * fact n <> 0.
Proof. apply Nat.neq_mul_0; split; apply fact_neq_0. Qed.

Lemma fact_mul2_neq_0 m n p : fact m * fact n * fact p <> 0.
Proof. apply Nat.neq_mul_0; split. apply fact_mul_neq_0. apply fact_neq_0. Qed.

Lemma fact_S_div n : fact (S n) / fact n = S n.
Proof. rewrite fact_S. apply nat_div_mul_sym, fact_neq_0. Qed.

Lemma fact_S_div_alt n : 0 < n -> fact n / fact (pred n) = n.
Proof.
intros Hn; rewrite <- (Nat.succ_pred n). apply fact_S_div.
apply Nat.neq_0_lt_0; assumption.
Qed.

Lemma fact_gt_1 n : 1 < n -> 1 < fact n.
Proof.
destruct n. easy.
intros; rewrite fact_S; apply nat_lt_1_mul_pos_sym. assumption.
apply fact_gt_0.
Qed.

Lemma fact_eq_1_equiv n : fact n = 1 <-> n = 0 \/ n = 1.
Proof.
apply iff_sym; split.
+ intros [Hn | Hn]; subst. apply fact_0. apply fact_1.
+ destruct (lt_dec 1 n) as [Hn | Hn]; intros Hn1.
  - contradict Hn1. apply not_eq_sym, Nat.lt_neq, fact_gt_1. assumption.
  - apply Nat.le_1_r. apply Nat.nlt_ge. assumption.
Qed.

(* "_wn1" means "when (something is) not 1", here "when n <> 1". *)
Lemma fact_eq_1_wn1 n : n <> 1 -> fact n = 1 -> n = 0.
Proof.
intros H1; rewrite fact_eq_1_equiv; intros [H2 | H2]; subst. 2: contradict H1.
all: reflexivity.
Qed.

Lemma fact_le m n : m <= n -> fact m <= fact n.
Proof.
intros H; induction H as [| n H IH]. apply Nat.le_refl.
rewrite fact_S. apply Nat.le_trans with (fact n). assumption.
apply nat_le_mul_S_r.
Qed.

(* "_w0" means "when (something is) 0", here "when m = 0". *)
Lemma fact_lt_w0 m n : m = 0 -> n <> 1 -> m < n -> fact m < fact n.
Proof.
intros H0 H1 H; subst. rewrite fact_0; apply fact_gt_1.
apply Nat.nle_gt. contradict H1. apply Nat.le_antisymm. assumption.
apply Nat.le_succ_l. assumption.
Qed.

Lemma fact_lt_S n : n <> 0 -> fact n < fact (S n).
Proof.
intros Hn. rewrite fact_S. apply nat_lt_mul_S_r. apply fact_gt_0.
apply Nat.neq_0_lt_0. assumption.
Qed.

(* "_wn0" means "when (something is) not 0", here "when m <> 0". *)
Lemma fact_lt_wn0 m n : m <> 0 -> m < n -> fact m < fact n.
Proof.
intros Hm H; induction H as [| n H IH]. apply fact_lt_S; assumption.
apply Nat.lt_trans with (fact n). assumption.
apply fact_lt_S. apply Nat.neq_0_lt_0, Nat.le_lt_trans with m.
+ apply Nat.le_0_l.
+ apply Nat.lt_le_trans with (S m). 2: assumption.
  apply Nat.lt_succ_diag_r.
Qed.

Lemma fact_lt m n : m + n <> 1 -> m < n -> fact m < fact n.
Proof.
intros H; destruct (Nat.eq_dec m 0).
+ subst. apply fact_lt_w0. reflexivity. assumption.
+ apply fact_lt_wn0. assumption.
Qed.

(* "_w0" means "when (something is) 0", here "when m * n = 0". *)
Lemma fact_inj_w0 m n : m * n = 0 -> m + n <> 1 -> fact m = fact n -> m = n.
Proof.
rewrite Nat.mul_eq_0. intros [Hm | Hn]; subst; rewrite fact_0.
+ intros. rewrite fact_eq_1_wn1; easy.
+ rewrite Nat.add_0_r. apply fact_eq_1_wn1.
Qed.

(* "_w0" means "when (something is) 0", here "when m * n <> 0". *)
Lemma fact_inj_wn0 m n : m <> 0 -> n <> 0 -> fact m = fact n -> m = n.
Proof.
intros Hm Hn H1. destruct (Nat.eq_dec m n) as [| H2]. assumption.
apply not_eq in H2. contradict H1. destruct H2 as [H | H].
+ apply Nat.lt_neq, fact_lt. apply nat_eq_add_1_contra_wn0. all: assumption.
+ apply not_eq_sym, Nat.lt_neq, fact_lt. apply nat_eq_add_1_contra_wn0.
  all: assumption.
Qed.

Lemma fact_inj m n : m + n <> 1 -> fact m = fact n -> m = n.
Proof.
intros H1. destruct (Nat.eq_dec (m * n) 0) as [H0 | H0].
+ apply fact_inj_w0; assumption.
+ apply Nat.neq_mul_0 in H0. destruct H0 as [Hm Hn].
  apply fact_inj_wn0; assumption.
Qed.

Lemma fact_mod_0 m n : m <= n -> (fact n) mod (fact m) = 0.
Proof.
intros H. apply Nat.mod_divides. apply fact_neq_0.
revert m H. induction n.
+ intros m H. apply Nat.le_0_r in H. subst. exists 1. reflexivity.
+ intros m H1. destruct (Nat.eq_dec m (S n)) as [H2 | H2].
  - subst. exists 1. apply eq_sym, Nat.mul_1_r.
  - destruct (IHn m) as [c Hc]. apply nat_le_S_wnl; assumption.
    exists (S n * c). rewrite fact_S. rewrite Hc.
    rewrite (Nat.mul_comm (S n)), Nat.mul_assoc. reflexivity.
Qed.

Lemma fact_divide n p : S p <= n -> Nat.divide (S p) (fact n).
Proof.
intros H1; induction n. contradict H1; apply Nat.nle_succ_0.
rewrite fact_S. destruct (le_lt_dec (S p) n) as [H2 | H2].
+ apply Nat.divide_mul_l. apply IHn. assumption.
+ replace (S p) with (S n). apply Nat.divide_factor_r.
  apply Nat.le_antisymm. 2: assumption.
  apply Nat.le_succ_l. assumption.
Qed.

End Fact_Facts.
