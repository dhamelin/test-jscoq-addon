(** * Divisibility exercises *)

Require Import DivPrelude.
From Coq Require Import Lia List.
Import ZArith.BinInt ListNotations ZArithRing Znumtheory.

Local Open Scope Z_scope.

Lemma exercise1 (n p : Z) : n mod 7 = 3 -> p mod 7 = 1 -> (7 | 2 * n + p * p).
Proof.
  intros hn hp.
  destruct (mod_decomp p 7 1) as [k hk]; try easy.
  destruct (mod_decomp n 7 3) as [k' hk']; try easy.
  subst n p.
  replace 
     (2 * (7 * k' + 3) + (7 * k + 1) * (7 * k + 1)) with 
     (7 * (7 * k * k + 2 * k + 2 * k' + 1)) by lia.
  apply Z.divide_factor_l.
Qed.

Lemma exercise1' (n p : Z) :
  n mod 7 = 3 -> p mod 7 = 1 -> (7 | 2 * n + p * p).
Proof.
  intros Hn Hp; apply mod_divide.
  rewrite add_mod, (mul_mod 2), (mul_mod p), Hn, Hp, Z.mul_1_r.
  reflexivity.
Qed.

(*
Lemma exercise2 : (17 | 35 ^ 228 + 84 ^ 501).
Proof.
  apply mod_divide; easy.
Qed.

Ça marche pour de vrai !
It really works!
*)

Lemma exercise2 : (17 | 35 ^ 22800 + 84 ^ 500001).
Proof.
  apply mod_divide.
  rewrite add_mod, (pow_mod 35), (pow_mod 84).
  replace (35 mod 17) with 1 by reflexivity.
  (* We don't really work on Z/nZ so can't replace 16 with -1 *)
  replace (84 mod 17) with (17 + - 1) by reflexivity.
  rewrite (pow_mod (17 + -1)).
  replace ((17 + -1) mod 17) with (-1 mod 17) by reflexivity.
  rewrite <-(pow_mod _ 500001).
  rewrite pow_m1_l_odd;
    [| exact (Pos2Z.is_nonneg _) | exists 250000; reflexivity].
  rewrite Z.pow_1_l by (exact (Pos2Z.is_nonneg _)). (* <- bad pow *)
  rewrite <-(add_mod), Z.add_opp_diag_r, mod_0_l.
  reflexivity.
Qed.

Definition abcabc (a b c : Digit) : Z :=
  a * 100000 + b * 10000 + c * 1000 + a * 100 + b * 10 + c.

Lemma exerciseIntro (a b c : Digit) : (91 | abcabc a b c).
Proof.
  apply (Z.divide_trans _ 1001).
  - exists 11; reflexivity.
  - replace 
      (abcabc a b c) with 
      ((a * 100 + b * 10 + c) * 1001).
    + exact (Z.divide_factor_r _ _).
    + unfold abcabc. lia.
Qed.

Lemma exerciseIntro' (a b c : Digit) : (91 | abcabc a b c).
Proof.
  exists (11 * (a * 100 + b * 10 + c)).
  unfold abcabc. (* lia now solves it *)
  assert (a * 100000 = (a * 100) * 1000) as ->. {
    rewrite <-Z.mul_assoc; reflexivity.
  }
  assert (b * 10000 = (b * 10) * 1000) as ->. {
    rewrite <-Z.mul_assoc; reflexivity.
  }
  rewrite <-2(Z.mul_add_distr_r _ _ 1000).
  rewrite <-!Z.add_assoc.
  rewrite <-Z.mul_succ_r, <-Z.add_1_r.
  replace (1000 + 1) with (91 * 11) by reflexivity.
  rewrite <-Z.mul_assoc, (Z.mul_comm 11), !Z.mul_assoc.
  reflexivity.
Qed.

(* NOTE: this one is quite slow *)
Theorem exercise63 : findall (fun x => ((78^115372 + 92^23238 + 106^35536) mod 7 = x)).
Proof.
  specialize (pow_mod 78 115372 7) as h1.
  remember (78 ^ 115372) as b1.
  replace (78 mod 7) with 1 in h1; [|reflexivity].
  rewrite Z.pow_1_l in h1; [|cbv; intro h; inversion h].

  specialize (pow_mod 92 23238 7) as h2.
  remember (92 ^ 23238) as b2.
  replace (92 mod 7) with 1 in h2; [|reflexivity].
  rewrite Z.pow_1_l in h2; [|cbv; intro h; inversion h].

  specialize (pow_mod 106 35536 7) as h3.
  remember (106 ^ 35536) as b3.
  replace (106 mod 7) with 1 in h3; [|reflexivity].
  rewrite Z.pow_1_l in h3; [|cbv; intro h; inversion h].

  assert (HG: (b1 + b2 + b3) mod 7 = 3).
  * rewrite (add_mod (b1 + b2) b3 7).
    rewrite (add_mod b1 b2 7).
    rewrite h1. rewrite h2. rewrite h3. reflexivity.
  * simpl. exists [3]. split.
    + apply Forall_forall.
      intros x hx.
      apply in_inv in hx. 
      destruct hx; [|contradiction]. now subst x.
    + intros x hx.
      constructor.
      symmetry.
      now rewrite <- hx.
Qed.

(* Remplacer 42 par le bon résultat (entre 0 et 6) puis le prouver. *)
Theorem exercise63' : (78 ^ 115372 + 92 ^ 23238 + 106 ^ 35536) mod 7 = 3. (* 42 *)
Proof.
  (* We need to hide the explicit constants to block reduction. *)
  remember 115372 as a eqn:def_a.
  remember 23238 as b eqn:def_b.
  remember 35536 as c eqn:def_c.
  rewrite add_mod, (add_mod (78 ^ a)).
  rewrite (pow_mod 78), (pow_mod 92), (pow_mod 106).
  replace (78 mod 7) with 1 by (reflexivity).
  replace (92 mod 7) with 1 by (reflexivity).
  replace (106 mod 7) with 1 by (reflexivity).
  rewrite 3!Z.pow_1_l by (subst; exact (Pos2Z.is_nonneg _)).
  reflexivity.
Qed.

Lemma lastDigit2 (n : Digits) : (2 | n) <-> (2 | last_digit n).
Proof.
  assert (forall k, (2 | 10 * k)) as key. {
    intros k; apply Z.divide_mul_l; exists 5; reflexivity.
  }
  rewrite (decimal_decomposition n); split; intros H.
  - apply Z.divide_add_cancel_r with (2 := H); exact (key _).
  - apply Z.divide_add_r; [exact (key _) | exact H].
Qed.

Lemma sumDigits3_aux (n : Digits) : (n mod 3 = (digits_sum n) mod 3).
Proof.
  induction n as [d | h t IH]; [reflexivity |].
  rewrite digits_sum_digits, Z_of_Digits_dcons, add_mod.
  rewrite mul_mod, pow_mod.
  replace (10 mod 3) with 1 by (reflexivity).
  rewrite Z.pow_1_l; [| exact (digits_len_nneg _)]. (* <- ugly *)
  rewrite <-(mul_mod), Z.mul_1_r, (add_mod h), IH; reflexivity.
Qed.

Lemma sumDigits3 (n : Digits) : (3 | n) <-> (3 | digits_sum n).
Proof.
  apply iff_trans with (1 := divide_mod_iff _ _); apply iff_sym.
  apply iff_trans with (1 := divide_mod_iff _ _).
  rewrite sumDigits3_aux; reflexivity.
Qed.

Lemma exercise119_aux (n : Z) : (6 | n) <-> (3 | n) /\ (2 | n).
Proof.
  split.
  - intros [k ->]; split; apply Z.divide_mul_r, mod_divide; reflexivity.
  - intros [[q Hq] H]; rewrite Hq in H |- *.
    assert (2 | q) as [q' ->]. {
      apply (Z.gauss _ 3 q); [rewrite Z.mul_comm; exact H | reflexivity].
    }
    rewrite <-Z.mul_assoc; replace (2 * 3) with 6 by (reflexivity).
    exact (Z.divide_factor_r _ _).
Qed.

Theorem exercise119 (n : Digits) :
  (6 | n) <-> (6 | (4 * ((digits_sum n) - (last_digit n)) + last_digit n)).
Proof.
  apply iff_trans with (1:=exercise119_aux n).
  rewrite and_iff_compat_r with (1 := sumDigits3 n).
  rewrite and_iff_compat_l with (1 := lastDigit2 n).
  split.
  - intros [H3 H2]; apply exercise119_aux; split.
    + replace (4 * (digits_sum n - last_digit n) + last_digit n) with
        (4 * digits_sum n + 3 * (- (last_digit n))) by ring.
      apply Z.divide_add_r.
      * apply Z.divide_mul_r; exact H3.
      * exact (Z.divide_factor_l _ _).
    + apply Z.divide_add_r; [| exact H2].
      apply Z.divide_mul_l; exists 2; reflexivity.
  - intros [H3 H2]%exercise119_aux; split.
    + replace (4 * (digits_sum n - last_digit n) + last_digit n) with
        (3 * ((digits_sum n) - (last_digit n)) + (digits_sum n)) in H3 by ring.
      apply Z.divide_add_cancel_r with (2 := H3); exact (Z.divide_factor_l _ _).
    + apply Z.divide_add_cancel_r with (2 := H2).
      apply Z.divide_mul_l; exists 2; reflexivity.
Qed.

Theorem exercise117 (n k q : Z) (h0n : 0 <= n) (h0k : 0 <= k) (h0q : 0 <= q)
  (hk : n = k * k) (hq : n = q * q * q) : n mod 7 = 0 \/ n mod 7 = 1.
Proof.
  assert (n mod 7 = 0 \/ n mod 7 = 1 \/ n mod 7 = 2 \/ n mod 7 = 4) as h1. {
    rewrite !hk, mul_mod.
    study ((k mod 7)) between 0 and 6 as h2; cycle 7. {
      pose proof (Z.mod_pos_bound k 7) as bounds; lia.
    }
    all: rewrite <-h2; tauto.
  }
  assert (n mod 7 = 0 \/ n mod 7 = 1 \/ n mod 7 = 6) as h2. {
    rewrite !hq, (mul_mod (q * q)), (mul_mod q q).
    study (q mod 7) between 0 and 6 as h2; cycle 7. {
      pose proof (Z.mod_pos_bound q 7) as bounds; lia.
    }
    all: rewrite <-h2; tauto.
  }
  lia.
Qed.

Lemma exercise_121_aux (x y p : Z) : prime p -> x * y = p -> (0 <= x) ->
    (x = p /\ y = 1) \/ (x = 1 /\ y = p).
Proof.
  intros Hp H Hx.
  assert (x | p) as Hxdp by (exists y; symmetry; rewrite Z.mul_comm; exact H).
  pose proof (prime_divisors p Hp x Hxdp) as [H' | [H' | [H' | H']]].
  - exfalso; lia.
  - right; split; [exact H' | rewrite H', Z.mul_1_l in H; exact H].
  - left; split; [exact H' |]; apply (Z.mul_reg_l _ _ x). {
      intros C; rewrite C in H'; apply not_prime_0; rewrite H'; exact Hp.
    }
    rewrite Z.mul_1_r; rewrite H' at 2; exact H.
  - exfalso; pose proof (prime_ge_2 p Hp) as pge2; lia.
Qed.

Theorem exercise121 :
  findall (fun (M : Z * Z) => let (x, y) := M in y ^ 2 = x ^ 2 - 13 /\
    0 <= y /\ 0 <= x /\ 13 <= x ^ 2).
Proof.
  exists (cons (7,6) nil); split.
  - (* TODO: not good, only Coq things *)
    apply Forall_forall; intros x [H1 | []].
    rewrite <- H1; lia.
  - intros [x y] [H1 [H2 [H3 H0]]]; constructor.
    assert ((x + y) * (x - y) = 13) as key by lia.
    assert (prime 13) as p13. {
      constructor; [lia |].
      intros n Hn; apply Zgcd_1_rel_prime.
      (* TODO: We absolutely need to solve this in a better way! *)
      assert (n = 1 \/ n = 2 \/ n = 3 \/ n = 4 \/ n = 5 \/ n = 6 \/ n = 7 \/
        n = 8 \/ n = 9 \/ n = 10 \/ n = 11 \/ n = 12) as T by (lia).
      repeat destruct T as [T | T]; rewrite T; reflexivity.
    }
    assert (0 <= x + y) as sum_nneg by (lia).
    pose proof (exercise_121_aux _ _ _ p13 key sum_nneg)
      as [[xeq yeq] | [xeq yeq]]; [| exfalso; lia].
    apply pair_equal_spec; lia.
Qed.

Theorem exercise28 (k : Z) (hk : 0 <= k) (x : Z) (hx : 0 <= x) :
  (x | 9 * k + 2) -> (x | 12 * k + 1) -> (x = 1) \/ (x = 5).
Proof.
  intros h1 h2.
  assert (prime 5) as p5. {
    (* TODO: do better *)
      constructor; [lia |].
      intros n Hn; apply Zgcd_1_rel_prime.
      assert (n = 1 \/ n = 2 \/ n = 3 \/ n = 4) as T by (lia).
      repeat destruct T as [T | T]; rewrite T; reflexivity.
  }
  enough (x | 5) as xd5. {
    pose proof (prime_divisors 5 p5 _ xd5); lia. 
  }
  apply (Z.divide_mul_r x 4 (9 * k + 2)) in h1.
  apply (Z.divide_mul_r x (-3) (12 * k + 1)) in h2.
  replace 5 with (4 * (9 * k + 2) + -3 * (12 * k + 1)).
  + now apply Z.divide_add_r.
  + lia.
Qed.

Theorem exercise31 : findall (fun n => (0 <= n) /\ (n | n + 8)).
Proof.
  exists [1; 2; 4; 8].
  split.
  - apply Forall_forall. intros x hx.
  atomize hx. 5: inversion hx.
  all: split; [lia | subst x; apply mod_divide; easy].
  - intros x [hx1 hx2].
  assert (h2 : (x | 8)). {
    apply (Z.divide_add_r x (x + 8) (-x)) in hx2 as h3.
    - replace (x + 8 + -x) with 8 in h3; try easy; lia.
    - apply Z.divide_opp_r. apply Z.divide_refl.
  }
  study x between 0 and 8 as h3.
  10: apply Z.divide_pos_le in h2; nia.
  all: subst x.
  1: now apply Z.divide_0_l in h2.
  1, 2, 4, 8: now repeat constructor.
  all: apply Z.mod_divide in h2; [now cbn in h2 | lia].
Qed.
