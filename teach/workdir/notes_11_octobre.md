# Réunion Liber Abaci
mer. 11 oct. 2023 13:58:02 CEST

présents : Sylvie Boldo, François Clément, Pierre Rousselin

Le but est d'y voir plus clair sur les envies et les objectifs de chacun, en
lien avec Liber Abaci.

Le but du défi LA est de promouvoir l'utilisation de Coq en licence dans les
cours de mathématiques. Comme sous-produit, ce défi devrait améliorer
l'utilisabilité de Coq à travers notamment :
- les interfaces utilisateur
- de meilleures bibliotèques
- des tutoriels et une meilleure documentation

Les aspects pédagogiques sont à prendre en compte :
- la pédagogie liée à l'apprentissage de l'outil Coq lui-même
- la pédagogie mathématique
Il est important aussi de se poser la question de l'apport de Coq pour les
étudiants. Parfois, des étudiants prouvent de façon très mécaniques certains
lemmes, sans apporter de sens à ce qu'ils font, il est donc important de
s'interroger sur les liens entre intuition mathématique et preuves formelles.

Dans le cadre de LA, SB souhaite se concentrer sur l'amélioration générale
(polissage, refactorisation, documentation) de la bibliothèque coq-num-analysis
de façon à pouvoir l'utiliser en enseignement, en particulier sur l'intégrale de
Lebesgue. FC propose de réaliser du matériel pédagogique (exercices classiques
de L3) en Coq sur l'intégrale de Lebesgue.

PR : on peut travailler avec une base composée de
- Reals
- Coquelicot
- Lebesgue
- Classic + certains axiomes (a priori au moins funext)
- un choix judicieux de tactiques
La question de savoir si l'on peut patcher Reals avec certains morceaux de
Coquelicot (qui n'ont été écrits que pour ça) se pose.

Avant de parler de L3, on peut regarder le contenu de la L1 :
- logique "naïve", ensembles "naïfs"
- naturels, récurrence
- arithmétique élémentaire (divisibilité, arithmétique modulaire, ...)
- réels, suites numériques
- fonctions numériques, continuité, dérivabilité
- analyse asymptotique des fonctions (o, O, équivalents)
- espaces vectoriels de dimension finie
- polynômes

On peut déjà essayer d'ajouter des choses sur l'arithmétique.
