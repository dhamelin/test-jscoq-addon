{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    emacs-coq.url = "github:HamelinDavid/emacs-coq";
  };
  
  outputs = { self, nixpkgs, flake-utils, emacs-coq, ... }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      coqPackages = pkgs.coqPackages_8_16;
      coqInputs = with coqPackages; [   ];
    in {
      devShell = pkgs.mkShell {
        packages = [
          pkgs.dune_3
          coqPackages.coq
          
          pkgs.texliveFull
          pkgs.texstudio
          (emacs-coq.outputs.packages.${system}.emacs-coq.override {
            modules = [{
              coq.package = coqPackages.coq;
              emacs.package = pkgs.emacs;
              emacs.ui.theme.zenburn.enable = false;
            }];
          })
        ];
      };
    });
}

