# Plan de travail établi à Orsay le ven. 22 sept. 2023

On part a priori sur visio/hybride à Paris.
Tâche 7 : création de contenu
Tâche 1 : collaborations avec les enseignants

Se rappeler que chaque mail peut a priori être forwardé.

1. Réfléchir à l'ordre du jour (SB + tout le monde)
  Idées :
  - présentation du défi
  - état de l'art
  - Lebesgue
  - coefficients du binôme
2. Écrire à Martin Bodin (PR)
3. Choisir une date (et un lieu)
  Idées pour le lieu : IRIF (voir avec Alexis Saurin)
4. Appel à tarte
  - sur la liste Liber Abaci
  - enseignants contactés par Martin Bodin
  - LAGA ? Irem Paris Nord ?
  - Stream "teaching [with] Coq" dans zulip ?
